<?php

namespace Drupal\telega\Service;

use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\FileInterface;
use Longman\TelegramBot\Request;

/**
 * Telega Common service.
 */
class Telega {

  //phpcs:ignore
  private string $downloadPath = '';

  /**
   * Construct.
   */
  public function __construct() {
    $this->downloadPath = 'telega_message/attachment';
  }

  /**
   * File from message.
   */
  public function parseMessageAttach(array $message) : array {
    if (TRUE) {
      return [];
    }
    $attachment = $this->getAttachment($message);
    // @todo all message types to yaml data
    // - картинки
    // - файлы
    // - ссылки
    // - стикеры
    // - видео
    // - аудио
    // - действия
    // - кнопки
    // - емоджи-большие
    // - geo
    // - контакт
    // - опрос
    // - групповые действия
    return [
      'attachment' => $attachment,
      'text' => $this->getMessageText($message),
    ];
  }

  /**
   * Get photo file.
   */
  private function getAttachment(array $message) :? FileInterface {
    $config = \Drupal::config('telega.settings');
    if (!$config->get('fs-enable')) {
      return NULL;
    }
    $file_id = $this->getAttachmentFileId($message);
    if (empty($file_id)) {
      return NULL;
    }

    $file = Request::getFile(['file_id' => $file_id]);
    if (!$file->isOk()) {
      return NULL;
    }
    elseif (!Request::downloadFile($file->getResult())) {
      return NULL;
    }
    $filepath = $this->downloadPath . '/' . $file->getResult()->getFilePath();
    $scheme = $config->get('fs-scheme') ?: 'public';
    $date = \Drupal::service('date.formatter')->format(time(), 'custom', 'Y/m/d');
    $directory = "$scheme://telega_message/attachment/$date/";
    \Drupal::service('file_system')
      ->prepareDirectory(
        $directory, FileSystemInterface:: CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS
      );
    $data = file_get_contents($filepath);
    $filename = \Drupal::service('file_system')
      ->basename($filepath);
    $file = \Drupal::service('file.repository')
      ->writeData($data, $directory . $filename, FileExists::Rename);
    unlink($filepath);
    return $file;
  }

  /**
   * Get message text.
   */
  private function getMessageText(array $message) :? string {
    return implode(PHP_EOL, [
      $this->getVal($message, 'caption'),
      $this->getVal($message, 'text'),
    ]);
  }

  /**
   * Get available attachment type.
   */
  private function getAttachmentType(array $message) :? string {
    $available_types = ['audio', 'document', 'photo', 'video', 'voice'];
    $message_keys = array_keys($message);
    $types = array_intersect($available_types, $message_keys);
    if (empty($types)) {
      return NULL;
    }
    return array_shift($types);
  }

  /**
   * Get attachment file_id.
   */
  private function getAttachmentFileId(array $message) :? string {
    $attachment_type = $this->getAttachmentType($message);
    if (empty($attachment_type)) {
      return NULL;
    }
    $attachment = $message[$attachment_type];
    if ($attachment_type == 'photo') {
      $attachment = end($attachment);
    }
    return $attachment['file_id'];
  }

  /**
   * Check Valuer in array.
   */
  private function getVal($data, $key) {
    $result = NULL;
    if (!empty($data[$key])) {
      $result = $data[$key];
    }
    return $result;
  }

  /**
   * Check Temp.
   */
  private function checkTempFile($file_id) {
    $download_path = implode('/', [
      \Drupal::service('file_system')->getTempDirectory(),
      hash('sha256', $file_id, FALSE),
    ]);
  }

}
