<?php

namespace Drupal\telega\Form;

/**
 * @file
 * Contains Drupal\telega_chatwoot\Form\Settings.
 */

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements the Chatwoot form controller.
 */
class Settings extends ConfigFormBase {

  /**
   * Build.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('telega.settings');
    $form['files'] = [
      '#type' => 'details',
      '#title' => $this->t('File save settings'),
      '#open' => TRUE,
    ];
    $form['files']['fs-enable'] = [
      '#title' => $this->t('Save files'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('fs-enable'),
    ];
    $form['files']['fs-scheme'] = [
      '#title' => $this->t('File system scheme'),
      '#type' => 'radios',
      '#required' => TRUE,
      '#options' => [
        'public',
        'private',
      ],
      '#default_value' => $config->get('fs-scheme'),
    ];
    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'telega_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['telega.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('telega.settings');
    $config
      ->set('fs-enable', $form_state->getValue('fs-enable'))
      ->set('fs-scheme', $form_state->getValue('fs-scheme'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
