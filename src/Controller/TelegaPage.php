<?php

namespace Drupal\telega\Controller;

/**
 * @file
 * Contains \Drupal\telega\Controller\TelegaPage.
 */

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller AppPage.
 */
class TelegaPage extends ControllerBase {

  /**
   * Render Page.
   */
  public function page(Request $request) {
    $entity_type = 'telega_bot';
    $storage = \Drupal::entityTypeManager()->getStorage($entity_type);
    $pre = "o.O";
    return [
      'text' => ['#markup' => "<pre>$pre</pre>"],
    ];
  }

  /**
   * Title.
   */
  public function pageTitle() {
    return $this->t('Telega');
  }

}
