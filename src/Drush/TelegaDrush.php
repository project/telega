<?php

namespace Drupal\telega\Drush;

use Drush\Commands\DrushCommands;

/**
 * A Drush commandfile.
 */
class TelegaDrush extends DrushCommands {

  /**
   * App main drush.
   *
   * @command telega
   * @usage telega
   */
  public function telegaDrushTest() {
    $this->output()->writeln("Hello Telega");
  }

}
