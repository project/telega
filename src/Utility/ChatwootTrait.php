<?php

namespace Drupal\telega\Utility;

use Longman\TelegramBot\Entities\Message;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;

/**
 * Redis Store Trait.
 */
trait ChatwootTrait {

  /**
   * Message.
   *
   * @var Longman\TelegramBot\Entities\Message|null
   */
  protected $message = NULL;

  /**
   * Use this method to send text messages. On success, the last sent Message is returned.
   *
   * Proxy to parent Longman\TelegramBot\Request::sendMessage().
   * All message responses are saved in `$extras['responses']`.
   * Custom encoding can be defined in `$extras['encoding']` (default: `mb_internal_encoding()`)
   * Custom splitting can be defined in `$extras['split']` (default: 4096)
   *     `$extras['split'] = null;` // force to not split message at all!
   *     `$extras['split'] = 200;`  // split message into 200 character chunks.
   *
   * @link https://core.telegram.org/bots/api#sendmessage
   *
   * @todo Splitting formatted text may break the message.
   * 6
   * @param array $data
   * @param array|null $extras
   *
   * @return \Longman\TelegramBot\Entities\ServerResponse
   *
   * @throws TelegramException
   */
  public function sendMessage(array $data, ?array &$extras = []): ServerResponse {
    $this->tryChatwoot($data['text'] ?? "");
    if (!empty($extras)) {
      return Request::sendMessage($data, $extras);
    }
    return Request::sendMessage($data);
  }

  /**
   * Send to ChatWoot.
   */
  private function tryChatwoot($text) : bool {
    $this->message = $this->getMessageEntity();
    if ($this->message) {
      $chat = $this->message->getChat()->getId();
      $inbox = $this->telegram->bot->getYamlParam('chatwoot/inbox');
      if (\Drupal::hasService('telega_chatwoot')) {
        \Drupal::service('telega_chatwoot')->createSystemMessage($text, $chat, $inbox);
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Set message.
   */
  private function getMessageEntity() :? Message {
    $message = $this->getMessage() ?: $this->getEditedMessage() ?: $this->getChannelPost() ?: $this->getEditedChannelPost();
    return $message;
  }

}
