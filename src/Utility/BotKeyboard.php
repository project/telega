<?php

namespace Drupal\telega\Utility;

use Longman\TelegramBot\Entities\Keyboard;

/**
 * Class BotInlineKeyboard.
 */
class BotKeyboard extends Keyboard {

  /**
   * {@inheritdoc}
   */
  public function __construct($data = []) {
    $data = call_user_func_array([$this, 'createFromParams'], $data);
    parent::__construct($data);
    // Remove any empty buttons.
    $this->{$this->getKeyboardType()} = array_filter($this->{$this->getKeyboardType()});
  }

}
