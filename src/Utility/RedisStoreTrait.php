<?php

namespace Drupal\telega\Utility;

/**
 * Redis Store Trait.
 */
trait RedisStoreTrait {

  /**
   * Store Set.
   */
  private function set(string $key, string $val) {
    if (\Drupal::hasService('redis')) {
      \Drupal::service('redis')->set($key, $val);
    }
    else {
      \Drupal::cache()->set("telega::$key", $val);
    }
  }

  /**
   * Store get.
   */
  private function get(string $key) {
    $result = "";
    if (\Drupal::hasService('redis')) {
      $result = \Drupal::service('redis')->get($key);
    }
    else {
      if ($cache = \Drupal::cache()->get("telega::$key")) {
        $result = $cache->data;
      }
    }
    return $result;
  }

  /**
   * Start Conversation.
   */
  public function startConversation() {
    $conversation = $this->getName();
    return $this->setVal('conversation', $conversation);
  }

  /**
   * Stop conversation.
   */
  public function stopConversation() {
    return $this->setVal('conversation', "");
  }

  /**
   * Set Val to Redis.
   */
  public function setVal(string $name, int|string $val) {
    $root = $this->getRoot();
    $key = "$root:$name";
    $this->set($key, "$val");
  }

  /**
   * Get value From Redis.
   */
  public function getVal(string $name) : NULL | string {
    $root = $this->getRoot();
    $key = "$root:$name";
    return $this->get($key);
  }

  /**
   * Root.
   */
  public function getRoot() : string {
    // $bot_id = $this->telegram->getBotId();
    $bot_name = $this->telegram->getBotUsername();
    $message = $this->getMessage();
    if (!$message) {
      $callback_query = $this->getCallbackQuery();
      $message = $callback_query->getMessage();
    }
    $chat_id = $message->getChat()->getId();
    $user_id = $message->getFrom()->getId();

    $key = $chat_id;
    if ($user_id != $chat_id && empty($callback_query)) {
      $key = "{$chat_id}:{$user_id}";
    }
    $root = "\$telega:{$bot_name}:{$key}";
    return $root;
  }

}
