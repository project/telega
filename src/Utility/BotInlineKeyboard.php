<?php

namespace Drupal\telega\Utility;

/**
 * Class Bot Inline Keyboard.
 */
class BotInlineKeyboard extends BotKeyboard {

  /**
   * If this keyboard is an inline keyboard.
   */
  public function isInlineKeyboard() : bool {
    return TRUE;
  }

}
