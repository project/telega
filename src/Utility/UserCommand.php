<?php

namespace Drupal\telega\Utility;

use Longman\TelegramBot\Commands\UserCommand as UCommand;
use Longman\TelegramBot\Entities\ServerResponse;

/**
 * User Command.
 */
abstract class UserCommand extends UCommand implements TelegaCommandInterface {

  use RedisStoreTrait;
  use ChatwootTrait;

  /**
   * Version.
   *
   * @var string
   */
  protected $version = '1.2.0';

  /**
   * Private Only FLAG.
   *
   * @var bool
   */
  protected $private_only = TRUE;

  /**
   * Name.
   *
   * @var string
   */
  protected $name = 'blank';

  /**
   * Description.
   *
   * @var string
   */
  protected $description = 'blank';

  /**
   * Usage.
   *
   * @var string
   */
  protected $usage = '/blank';

  /**
   * Need Mysql.
   *
   * @var bool
   */
  protected $need_mysql = FALSE;

  /**
   * {@inheritdoc}
   */
  public function replyToChat(string $text, array $data = []): ServerResponse {
    $this->tryChatwoot($text);
    return parent::replyToChat($text, $data);
  }

}
