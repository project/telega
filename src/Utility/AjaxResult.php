<?php

namespace Drupal\telega\Utility;

/**
 * @file
 * Contains \Drupal\telega\Utility\AjaxResult.
 */

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;

/**
 * Ajax Heloper.
 */
class AjaxResult {

  /**
   * AJAX Responce.
   */
  public static function ajax(string $wrapper, string $otvet, bool|array $commands = FALSE) : AjaxResponse {
    $output = '';
    if ($otvet || $commands) {
      $output .= '<pre>';
      $output .= $otvet;
      if ($commands) {
        $output .= implode("\n", $commands);
      }
      $output .= '</pre>';
    }
    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand("#" . $wrapper, $output));
    return $response;
  }

  /**
   * AJAX Button.
   */
  public static function button(string $function, string $button = "Send", string $color = 'primary') : array {
    return [
      '#type' => 'submit',
      '#value' => $button,
      '#attributes' => ['class' => ['btn', 'btn-xs', 'btn-' . $color]],
      '#ajax'   => [
        'callback' => $function,
        'effect'   => 'fade',
        'progress' => ['type' => 'throbber', 'message' => ""],
      ],
    ];
  }

  /**
   * AJAX Button.
   */
  public static function select(string $function, array $options, string $default) {
    $default_value = FALSE;
    if (isset($options[$default])) {
      $default_value = $default;
    }
    else {
      $default_value = 'select';
      $options['select'] = 'Select';
    }
    return [
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $default_value,
      '#ajax'   => [
        'callback' => $function,
        'effect'   => 'fade',
        'progress' => ['type' => 'throbber', 'message' => ""],
      ],
    ];
  }

}
