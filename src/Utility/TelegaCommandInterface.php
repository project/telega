<?php

namespace Drupal\telega\Utility;

use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Telegram;

/**
 * Provides command interface KO!.
 */
interface TelegaCommandInterface {

  /**
   * Constructor.
   *
   * @param \Longman\TelegramBot\Telegram $telegram
   *   Telegram.
   * @param \Longman\TelegramBot\Entities\Update|null $update
   *   Update.
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL);

  /**
   * Main command execution.
   *
   * @return \Longman\TelegramBot\Entities\ServerResponse
   *   Teleega responce.
   *
   * @throws \Longman\TelegramBot\Exception\TelegramException
   */
  public function execute(): ServerResponse;

}
