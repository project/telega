<?php

namespace Drupal\telega_session\Service;

use danog\MadelineProto\API;
use danog\MadelineProto\Logger;
use danog\MadelineProto\Settings;
use Drupal\telega_session\Entity\TelegaSession;
use Drupal\telega_session\Utility\SessionDb;
use Symfony\Component\Dotenv\Dotenv;

/**
 * Session service.
 */
class Session {

  //phpcs:disable
  private string $phone;
  private Settings $settings;
  private TelegaSession $entity;
  //phpcs:enable

  /**
   * Creates a new Session manager.
   */
  public function __construct() {
    if ($_SERVER['SHELL'] ?? FALSE) {
      // Is drush.
      return;
    }
    $dotenv = new Dotenv();
    $dotenv->load(DRUPAL_ROOT . '/.env');
    $db = SessionDb::redisSettings();
    // error_reporting(0);.
    $settings = new Settings();
    $settings->setDb($db);
    $settings->getAppInfo()->setApiId((int) $_ENV['TG_APP_ID']);
    $settings->getAppInfo()->setApiHash($_ENV['TG_APP_KEY']);
    // dsm($settings);
    $date = date('dmY');
    $settings->getLogger()->setLevel(Logger::DEFAULT_LOGGER)->setExtra("log/madeline_proto_$date.log");
    error_reporting(0);
    $this->settings = $settings;
  }

  /**
   * Init whith entity or IF .
   */
  public function init(TelegaSession|int $entity) {
    if (is_int($entity)) {
      $entity = TelegaSession::load($entity);
    }
    $this->entity = $entity;
    $this->phone = $entity->phone->value;
    return $this;
  }

  /**
   * Code request.
   */
  public function sendMessage($text) {
    $mdProto = new API($this->sessionFile(), $this->settings);
    $mdProto->async(FALSE);
    $mdProto->start();
    $mdProto->messages->sendMessage([
      'peer' => $_ENV['TELEGA_USER'],
      'message' => "$text " . date("dM H:i:s"),
    ]);
    $mdProto->stop();
    error_reporting(0);
  }

  /**
   * Code request.
   */
  public function listMessages() : string {
    $mdProto = new API($this->sessionFile(), $this->settings);
    $mdProto->async(FALSE);
    $mdProto->start();
    $messages = $mdProto->messages->getHistory([
    // Название канала, без @.
      'peer' => 'politsin',
      'offset_id' => 0,
      'offset_date' => 0,
      'add_offset' => 0,
      'limit' => '10',
      'max_id' => 999999999,
      'min_id' => 0,
      'hash' => 0,
    ]);
    $output = "";
    foreach ($messages['messages'] as $i => $msg) {
      $output .= "{$msg['message']}\n";
    }
    $mdProto->stop();
    error_reporting(0);
    return $output;
  }

  /**
   * Code request.
   */
  public function requestCode() {
    $mdProto = new API($this->sessionFile(), $this->settings);
    $mdProto->async(FALSE);
    $mdProto->phoneLogin($this->phone);
    error_reporting(0);
  }

  /**
   * Code confirm.
   */
  public function confirmCode($code) {
    $mdProto = new API($this->sessionFile(), $this->settings);
    $mdProto->async(FALSE);
    $mdProto->phoneLogin($this->phone);
    $mdProto->completePhoneLogin($code);
    error_reporting(0);
  }

  /**
   * Run.
   */
  public function runAsync() {
    $mdProto = new API($this->sessionFile(), $this->settings);
    $mdProto->async(FALSE);
    // $me = $mdProto->fullGetSelf();
    // dsm($me);
    $mdProto->start();
    $mdProto->messages->sendMessage([
      'peer' => '++',
      'message' => "hello " . date("dM H:i:s"),
    ]);
    $mdProto->stop();
    error_reporting(0);
    // dsm($mdProto);
    return TRUE;
  }

  /**
   * Session directory.
   */
  private function sessionFile() : string {
    $filesystem = \Drupal::service('file_system');
    $path = $filesystem->realpath('private://');
    $dir = "$path/telega/session/$this->phone";
    if (!is_dir($dir)) {
      mkdir($dir, 0755, TRUE);
    }
    return "$dir/session";
  }

}
