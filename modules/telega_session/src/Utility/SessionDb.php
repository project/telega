<?php

namespace Drupal\telega_session\Utility;

use danog\MadelineProto\Settings\Database\Redis;
use danog\MadelineProto\Settings\Database\Mysql;
use danog\MadelineProto\Settings\DatabaseAbstract;

/**
 * Session service.
 */
class SessionDb {

  /**
   * Get Db.
   */
  public static function dbSettings() : DatabaseAbstract {
    $db = self::redisSettings();
    if ($_ENV['REDIS']) {
      // @todo $db = self::redisSettings();
    }
    return $db;
  }

  /**
   * Redis config. Default DB1 (not the DB0)
   */
  public static function redisSettings(int $db = 1) : DatabaseAbstract {
    $database = new Redis();
    $database->setUri($_ENV['REDIS_URI']);
    $database->setDatabase($db);
    $database->setPassword($_ENV['REDIS_PASS']);
    return $database;
  }

  /**
   * MySQL config.
   */
  public static function mySqlSettings() : DatabaseAbstract {
    $database = new Mysql();
    $database->setUri($_ENV['MySQL_HOST']);
    $database->setDatabase($_ENV['MySQL_DB']);
    $database->setUsername($_ENV['MySQL_USER']);
    $database->setPassword($_ENV['MySQL_PASS']);
    return $database;
  }

}
