<?php

namespace Drupal\telega_session\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the session entity edit forms.
 */
class TelegaSessionForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + [
      'link' => \Drupal::service('renderer')->render($link),
    ];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New session %label has been created.', $message_arguments));
      $this->logger('telega_session')->notice('Created new session %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The session %label has been updated.', $message_arguments));
      $this->logger('telega_session')->notice('Updated new session %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.telega_session.canonical', ['telega_session' => $entity->id()]);
  }

}
