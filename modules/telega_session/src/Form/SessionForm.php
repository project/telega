<?php

namespace Drupal\telega_session\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\telega\Utility\AjaxResult;

/**
 * Implements the form controller.
 */
class SessionForm extends FormBase {

  /**
   * Ajax Wrapper.
   *
   * @var string
   */
  private $wrapper = 'telega-session-form-wrap';

  /**
   * AJAX Version.
   */
  public function ajaxSetup(array &$form, $form_state) {
    $entity = $form_state->entity;
    $name = $entity->title->value;
    $otvet = "setup $name:\n";
    $session = \Drupal::service('telega_session')->init($entity);
    $otvet .= $session->runAsync();
    return AjaxResult::ajax($this->wrapper, $otvet);
  }

  /**
   * List last messages.
   */
  public function ajaxMsgList(array &$form, $form_state) {
    $entity = $form_state->entity;
    $name = $entity->title->value;
    $otvet = "Last Messages [$name]:\n";
    $otvet .= \Drupal::service('telega_session')->init($entity)->listMessages();
    return AjaxResult::ajax($this->wrapper, $otvet);
  }

  /**
   * Submit Message.
   */
  public function ajaxMsgSubmit(array &$form, $form_state) {
    $entity = $form_state->entity;
    $name = $entity->title->value;
    $otvet = "Submit Message [$name]:\n";
    $text = $form_state->getValue('message-text');
    $otvet .= $text;
    \Drupal::service('telega_session')->init($entity)->sendMessage($text);
    return AjaxResult::ajax($this->wrapper, $otvet);
  }

  /**
   * Build the simple form.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $extra = NULL) {
    $wrap = $this->wrapper;
    $form_state->entity = $extra;
    $form_state->setCached(FALSE);
    $phone = $extra->phone->value;
    $form['login'] = [
      '#type' => 'details',
      '#title' => $this->t('Login'),
      '#open' => FALSE,
      'session-id' => [
        '#type' => 'hidden',
        '#value' => $extra->id(),
      ],
      'session-phone' => [
        '#type' => 'hidden',
        '#value' => $phone,
      ],
      'submits' => [
        '#type' => 'container',
        'code-request' => [
          '#type' => 'submit',
          '#value' => "Login with $phone",
          '#attributes' => ['class' => ['btn', 'btn-xs', 'btn-warning']],
          '#ajax'   => [
            'callback' => '::codeCallback',
            'wrapper'  => 'd-wrapper',
            'effect'   => 'none',
            'progress' => ['type' => 'throbber', 'message' => NULL],
          ],
        ],
      ],
      'code-wrapper' => [
        '#type' => 'container',
        '#prefix' => '<div id="d-wrapper">',
        '#suffix' => '</div>',
        'code' => [
          '#type' => 'hidden',
        ],
      ],

    ];
    $form['message'] = [
      '#type' => 'details',
      '#title' => $this->t('Message'),
      '#open' => TRUE,
      'message-text' => [
        '#type' => 'textfield',
      ],
      'submits' => [
        '#type' => 'container',
        'message-list' => AjaxResult::button('::ajaxMsgList', 'Last Messages'),
        'message-submit' => AjaxResult::button('::ajaxMsgSubmit', 'Send Message'),
      ],
    ];
    $from['session-phone'] = [
      '#type' => 'hidden',
      '#value' => $phone,
    ];
    $from['session-id'] = [
      '#type' => 'hidden',
      '#value' => $extra->id(),
    ];
    $form['output'] = [
      '#suffix' => "<div id='$wrap'>console</div>",
    ];
    return $form;
  }

  /**
   * Implements codeCallback.
   */
  public function codeCallback(array &$form, $form_state) {
    $flag = $form_state->getValue('code-checkbox');
    $sid = $form_state->getValue('session-id');
    if (TRUE || $flag != 1) {
      \Drupal::service('telega_session')->init($sid)->requestCode();
      $form['login']['code-wrapper'] = [
        'code' => [
          '#title' => $this->t("Code") . " $sid",
          '#type' => 'textfield',
          '#name' => 'code',
          '#description' => 'Код авторизации',
        ],
        'login-submit' => [
          '#type' => 'container',
          'code-submit' => AjaxResult::button('::ajaxSetup', 'Submit Code', 'success'),
        ],
      ];
    }
    $form_state->setRebuild(TRUE);
    return $form['login']['code-wrapper'];
  }

  /**
   * Implements a form submit handler.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $code = $form_state->getValue('code');
    if (!$code) {
      \Drupal::messenger()->addWarning("Code miss");
    }
    else {
      $sid = $form_state->getValue('session-id');
      $phone = $form_state->getValue('session-phone');
      \Drupal::messenger()->addStatus("$code / $phone");
      \Drupal::service('telega_session')->init($sid)->confirmCode($code);
    }
    $form_state->setRebuild(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'telega_session_form';
  }

}
