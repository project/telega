<?php

namespace Drupal\telega_session\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Provides a view controller for TelegaSession entity type.
 */
class TelegaSessionViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getBuildDefaults(EntityInterface $entity, $view_mode) {
    $build = parent::getBuildDefaults($entity, $view_mode);
    if ($view_mode == 'full') {
      $f = 'Drupal\telega_session\Form';
      $from = \Drupal::formBuilder()->getForm("$f\SessionForm", $entity);
      $build['form'] = $from;
    }
    return $build;
  }

}
