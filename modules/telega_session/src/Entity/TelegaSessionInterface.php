<?php

namespace Drupal\telega_session\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a session entity type.
 */
interface TelegaSessionInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the session title.
   *
   * @return string
   *   Title of the session.
   */
  public function getTitle();

  /**
   * Sets the session title.
   *
   * @param string $title
   *   The session title.
   *
   * @return \Drupal\telega_session\TelegaSessionInterface
   *   The called session entity.
   */
  public function setTitle($title);

  /**
   * Gets the session creation timestamp.
   *
   * @return int
   *   Creation timestamp of the session.
   */
  public function getCreatedTime();

  /**
   * Sets the session creation timestamp.
   *
   * @param int $timestamp
   *   The session creation timestamp.
   *
   * @return \Drupal\telega_session\TelegaSessionInterface
   *   The called session entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the session status.
   *
   * @return bool
   *   TRUE if the session is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the session status.
   *
   * @param bool $status
   *   TRUE to enable this session, FALSE to disable.
   *
   * @return \Drupal\telega_session\TelegaSessionInterface
   *   The called session entity.
   */
  public function setStatus($status);

}
