<?php

namespace Drupal\telega_session\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\telega\Utility\ContentEntity;
use Drupal\telega\Utility\FieldDefinition;

/**
 * Defines the session entity class.
 *
 * @ContentEntityType(
 *   id = "telega_session",
 *   label = @Translation("Session"),
 *   label_collection = @Translation("Sessions"),
 *   handlers = {
 *     "view_builder" = "Drupal\telega_session\Entity\TelegaSessionViewBuilder",
 *     "list_builder" = "Drupal\telega_session\Entity\TelegaSessionListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\telega_session\Form\TelegaSessionForm",
 *       "edit" = "Drupal\telega_session\Form\TelegaSessionForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "telega_session",
 *   admin_permission = "administer session",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/telega/session/add",
 *     "canonical" = "/telega/session/{telega_session}",
 *     "edit-form" = "/telega/session/{telega_session}/edit",
 *     "delete-form" = "/telega/session/{telega_session}/delete",
 *     "collection" = "/telega/session"
 *   },
 *   field_ui_base_route = "entity.telega_session.settings"
 * )
 */
class TelegaSession extends ContentEntity implements TelegaSessionInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['title'] = FieldDefinition::string('Title');
    $fields['status'] = FieldDefinition::status();
    $fields['uid'] = FieldDefinition::uid();
    $fields['created'] = FieldDefinition::created();
    $fields['changed'] = FieldDefinition::changed();
    // Custom.
    $fields['key'] = FieldDefinition::bigint('Key');
    $fields['name'] = FieldDefinition::string('Name');
    $fields['phone'] = FieldDefinition::string('Phone');
    $fields['token'] = FieldDefinition::string('Token');
    $fields['yaml'] = FieldDefinition::long('Yaml');
    $fields['data'] = FieldDefinition::map('Extra');
    return $fields;
  }

}
