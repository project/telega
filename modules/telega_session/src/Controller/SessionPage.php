<?php

namespace Drupal\telega_session\Controller;

/**
 * @file
 * Contains \Drupal\telega_session\Controller\SessionPage.
 */

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller ChatPage.
 */
class SessionPage extends ControllerBase {

  /**
   * Render Page.
   */
  public function page(Request $request) {
    return [
      'text' => ['#markup' => "<pre>==</pre>"],
    ];
  }

}
