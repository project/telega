<?php

namespace Drupal\telega_message\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a message entity type.
 */
interface TelegaMessageInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the message title.
   *
   * @return string
   *   Title of the message.
   */
  public function getTitle();

  /**
   * Sets the message title.
   *
   * @param string $title
   *   The message title.
   *
   * @return \Drupal\telega_message\TelegaMessageInterface
   *   The called message entity.
   */
  public function setTitle($title);

  /**
   * Gets the message creation timestamp.
   *
   * @return int
   *   Creation timestamp of the message.
   */
  public function getCreatedTime();

  /**
   * Sets the message creation timestamp.
   *
   * @param int $timestamp
   *   The message creation timestamp.
   *
   * @return \Drupal\telega_message\TelegaMessageInterface
   *   The called message entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the message status.
   *
   * @return bool
   *   TRUE if the message is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the message status.
   *
   * @param bool $status
   *   TRUE to enable this message, FALSE to disable.
   *
   * @return \Drupal\telega_message\TelegaMessageInterface
   *   The called message entity.
   */
  public function setStatus($status);

}
