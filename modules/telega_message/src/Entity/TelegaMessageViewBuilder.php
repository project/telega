<?php

namespace Drupal\telega_message\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Provides a view controller for TelegaMessage entity type.
 */
class TelegaMessageViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getBuildDefaults(EntityInterface $entity, $view_mode) {
    $build = parent::getBuildDefaults($entity, $view_mode);
    if ($view_mode == 'full') {
      $f = 'Drupal\telega_message\Form';
      $from = \Drupal::formBuilder()->getForm("$f\MessageForm", $entity);
      $build['form'] = $from;
    }
    return $build;
  }

}
