<?php

namespace Drupal\telega_message\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\telega\Utility\ContentEntity;
use Drupal\telega\Utility\FieldDefinition;
use Drupal\Core\Field\FieldStorageDefinitionInterface;

/**
 * Defines the message entity class.
 *
 * @ContentEntityType(
 *   id = "telega_message",
 *   label = @Translation("Message"),
 *   label_collection = @Translation("Messages"),
 *   handlers = {
 *     "view_builder" = "Drupal\telega_message\Entity\TelegaMessageViewBuilder",
 *     "list_builder" = "Drupal\telega_message\Entity\TelegaMessageListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\telega_message\Form\TelegaMessageForm",
 *       "edit" = "Drupal\telega_message\Form\TelegaMessageForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "telega_message",
 *   admin_permission = "administer message",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/telega/message/add",
 *     "canonical" = "/telega/message/{telega_message}",
 *     "edit-form" = "/telega/message/{telega_message}/edit",
 *     "delete-form" = "/telega/message/{telega_message}/delete",
 *     "collection" = "/telega/message"
 *   },
 *   field_ui_base_route = "entity.telega_message.settings"
 * )
 */
class TelegaMessage extends ContentEntity implements TelegaMessageInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['title'] = FieldDefinition::string('Title');
    $fields['status'] = FieldDefinition::status();
    $fields['uid'] = FieldDefinition::uid();
    $fields['created'] = FieldDefinition::created();
    $fields['changed'] = FieldDefinition::changed();
    // Custom.
    $fields['key'] = FieldDefinition::bigint('Key');
    $fields['date'] = FieldDefinition::date('Date');
    $fields['bot_id'] = FieldDefinition::entity('Bot', 'telega_bot');
    $fields['chat_id'] = FieldDefinition::entity('Chat', 'telega_chat');
    $fields['user_id'] = FieldDefinition::entity('User', 'telega_user');
    $fields['reply_to_message_id'] = FieldDefinition::entity('Reply to message', 'telega_message');
    $fields['attachment'] = FieldDefinition::file('Attachment')
      ->setCardinality(
        FieldStorageDefinitionInterface::CARDINALITY_UNLIMITED
      );
    $fields['text'] = FieldDefinition::long('Text');
    $fields['yaml'] = FieldDefinition::long('Yaml');
    $fields['entities'] = FieldDefinition::map('Entities');
    $fields['data'] = FieldDefinition::map('Extra');
    return $fields;
  }

}
