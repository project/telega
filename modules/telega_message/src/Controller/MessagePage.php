<?php

namespace Drupal\telega_message\Controller;

/**
 * @file
 * Contains \Drupal\telega_message\Controller\MessagePage.
 */

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller MessagePage.
 */
class MessagePage extends ControllerBase {

  /**
   * Render Page.
   */
  public function page(Request $request) {
    return [
      'text' => ['#markup' => "<pre>==</pre>"],
    ];
  }

}
