<?php

namespace Drupal\telega_message\Service;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\telega_message\Entity\TelegaMessage;

/**
 * Message service.
 */
class Message {

  use LoggerChannelTrait;
  use MessengerTrait;
  use StringTranslationTrait;

  //phpcs:disable
  protected EntityStorageInterface $storage;
  protected EntityTypeInterface $entityType;
  protected DateFormatter $dateFormatter;
  //phpcs:enable

  /**
   * Creates a new Message manager.
   */
  public function __construct() {
    $entity_type = 'telega_message';
    $this->storage = \Drupal::entityTypeManager()->getStorage($entity_type);
    $this->entityType = \Drupal::entityTypeManager()->getDefinition($entity_type);
    $this->dateFormatter = \Drupal::service('date.formatter');
  }

  /**
   * Creates a new Message manager.
   */
  public function renderLast(EntityInterface $chat) {
    $title = t('Last messages');
    $rows = [];
    $no = 0;
    foreach ($this->query($chat->id()) as $message) {
      /** @var \Drupal\telega_message\Entity\TelegaMessage $device */
      $rows[] = $this->buildRow($message, ++$no);
    }
    $build['title'] = [
      '#markup' => "<h4>$title</h4>",
    ];
    $build['table'] = [
      '#type' => 'table',
      '#header' => $this->buildHeader(),
      '#title' => $this->t('credentials'),
      '#rows' => $rows,
      '#empty' => $this->t('There are no @label yet.', [
        '@label' => $this->entityType->getPluralLabel(),
      ]),
      '#cache' => [
        'contexts' => $this->entityType->getListCacheContexts(),
        'tags' => $this->entityType->getListCacheTags(),
      ],
    ];
    return $build;
  }

  /**
   * Header.
   */
  private function buildHeader() {
    $header['no'] = $this->t('#');
    $header['name'] = $this->t('Name');
    $header['text'] = $this->t('Text');
    $header['chat'] = $this->t('Chat');
    $header['user'] = $this->t('User');
    $header['created'] = $this->t('Created');
    return $header;
  }

  /**
   * Row.
   */
  private function buildRow(TelegaMessage $entity, $no = FALSE) {
    $row['no'] = $no;
    $row['name'] = $entity->toLink();
    $row['text'] = $entity->text->value;
    $row['chat'] = '';
    $chat = $entity->chat_id->entity;
    if ($chat) {
      $id = $chat->id();
      if ($chat->key->value < 0) {
        $lable = $entity->chat_id->entity->label();
        $row['chat'] = [
          'data' => [
            '#markup' => "<a href='/telega/chat/$id'>$lable</a>",
          ],
        ];
      }
      else {
        $row['chat'] = [
          'data' => [
            '#markup' => "<a href='/telega/chat/$id'>👤</a>",
          ],
        ];
      }
    }

    $row['user'] = [
      'data' => [
        '#markup' => $entity->user_id->entity->name->value,
      ],
    ];
    $row['created'] = $this->dateFormatter->format($entity->getCreatedTime());
    return $row;
  }

  /**
   * Query.
   */
  private function query($chat_id) {
    $entities = [];
    $query = $this->storage->getQuery()
      ->condition('status', 1)
      ->condition('chat_id', $chat_id)
      ->sort('created', 'DESC')
      ->accessCheck(TRUE)
      ->range(0, 25);
    $ids = $query->execute();
    if (!empty($ids)) {
      foreach ($this->storage->loadMultiple($ids) as $id => $entity) {
        $entities[$id] = $entity;
      }
    }
    return $entities;
  }

}
