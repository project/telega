<?php

namespace Drupal\telega_user\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Provides a view controller for TelegaUser entity type.
 */
class TelegaUserViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getBuildDefaults(EntityInterface $entity, $view_mode) {
    $build = parent::getBuildDefaults($entity, $view_mode);
    if ($view_mode == 'full') {
      $f = 'Drupal\telega_user\Form';
      $from = \Drupal::formBuilder()->getForm("$f\UserForm", $entity);
      $build['form'] = $from;
    }
    return $build;
  }

}
