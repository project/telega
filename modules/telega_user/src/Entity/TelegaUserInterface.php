<?php

namespace Drupal\telega_user\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining an user entity type.
 */
interface TelegaUserInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the user title.
   *
   * @return string
   *   Title of the user.
   */
  public function getTitle();

  /**
   * Sets the user title.
   *
   * @param string $title
   *   The user title.
   *
   * @return \Drupal\telega_user\TelegaUserInterface
   *   The called user entity.
   */
  public function setTitle($title);

  /**
   * Gets the user creation timestamp.
   *
   * @return int
   *   Creation timestamp of the user.
   */
  public function getCreatedTime();

  /**
   * Sets the user creation timestamp.
   *
   * @param int $timestamp
   *   The user creation timestamp.
   *
   * @return \Drupal\telega_user\TelegaUserInterface
   *   The called user entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the user status.
   *
   * @return bool
   *   TRUE if the user is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the user status.
   *
   * @param bool $status
   *   TRUE to enable this user, FALSE to disable.
   *
   * @return \Drupal\telega_user\TelegaUserInterface
   *   The called user entity.
   */
  public function setStatus($status);

}
