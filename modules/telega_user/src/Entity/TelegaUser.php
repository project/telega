<?php

namespace Drupal\telega_user\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\telega\Utility\ContentEntity;
use Drupal\telega\Utility\FieldDefinition;

/**
 * Defines the user entity class.
 *
 * @ContentEntityType(
 *   id = "telega_user",
 *   label = @Translation("User"),
 *   label_collection = @Translation("Users"),
 *   handlers = {
 *     "view_builder" = "Drupal\telega_user\Entity\TelegaUserViewBuilder",
 *     "list_builder" = "Drupal\telega_user\Entity\TelegaUserListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\telega_user\Form\TelegaUserForm",
 *       "edit" = "Drupal\telega_user\Form\TelegaUserForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "telega_user",
 *   admin_permission = "administer telega-user",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form"    = "/telega/user/add",
 *     "canonical"   = "/telega/user/{telega_user}",
 *     "edit-form"   = "/telega/user/{telega_user}/edit",
 *     "delete-form" = "/telega/user/{telega_user}/delete",
 *     "collection"  = "/telega/user"
 *   },
 *   field_ui_base_route = "entity.telega_user.settings"
 * )
 */
class TelegaUser extends ContentEntity implements TelegaUserInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['title'] = FieldDefinition::string('Title');
    $fields['status'] = FieldDefinition::status();
    $fields['uid'] = FieldDefinition::uid();
    $fields['created'] = FieldDefinition::created();
    $fields['changed'] = FieldDefinition::changed();
    // Custom.
    $fields['key'] = FieldDefinition::bigint('Key');
    $fields['bot'] = FieldDefinition::bool('Bot');
    $fields['name'] = FieldDefinition::string('Name');
    $fields['first_name'] = FieldDefinition::string('First Name');
    $fields['last_name'] = FieldDefinition::string('Last Name');
    $fields['lang'] = FieldDefinition::string('Language');
    $fields['yaml'] = FieldDefinition::long('Yaml');
    $fields['data'] = FieldDefinition::map('Extra');
    return $fields;
  }

}
