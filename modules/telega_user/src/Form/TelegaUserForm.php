<?php

namespace Drupal\telega_user\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the user entity edit forms.
 */
class TelegaUserForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + [
      'link' => \Drupal::service('renderer')->render($link),
    ];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New user %label has been created.', $message_arguments));
      $this->logger('telega_user')->notice('Created new user %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The user %label has been updated.', $message_arguments));
      $this->logger('telega_user')->notice('Updated new user %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.telega_user.canonical', ['telega_user' => $entity->id()]);
  }

}
