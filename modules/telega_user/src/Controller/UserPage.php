<?php

namespace Drupal\telega_user\Controller;

/**
 * @file
 * Contains \Drupal\telega_user\Controller\UserPage.
 */

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller ChatPage.
 */
class UserPage extends ControllerBase {

  /**
   * Render Page.
   */
  public function page(Request $request) {
    return [
      'text' => ['#markup' => "<pre>==</pre>"],
    ];
  }

}
