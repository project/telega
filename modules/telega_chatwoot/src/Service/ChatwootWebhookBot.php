<?php

namespace Drupal\telega_chatwoot\Service;

use Drupal\Component\Serialization\Json;
use Drupal\telega_bot\Entity\TelegaBot;

/**
 * Chatwoot service.
 *
 * Сообщения от операторов из чатвута.
 */
class ChatwootWebhookBot {

  // phpcs:disable
  const DEBUG = FALSE;
  private TelegaBot $botEntity;
  // phpcs:enable

  /**
   * WebHook - все особытия в чатвуте.
   */
  public function webhook(TelegaBot $bot_entity, array $data) : void {
    $this->botEntity = $bot_entity;
    // $this->debug(json_encode($data));
    $message_type = $data['message_type'] ?? "";
    if ($message_type == 'outgoing' && !$data['private']) {
      // Сообщение от оператора (исходящие не приватные).
      if ($chat = $this->chatId($data)) {
        $this->operatorMessage($chat, $data);
      }
    }
  }

  /**
   * Outgouing operator message for user.
   */
  public function operatorMessage(string $chat, array $data) : void {
    $message = $data['content'];
    $bot = \Drupal::service('telega_bot')
      ->init($this->botEntity)
      ->setSkipChatwoot(TRUE);
    if (is_string($message)) {
      // Сообщение оператора из чатвута отправляем в телегу юзеру.
      $bot->send($chat, $message);
    }
    // Отправляем прикрепленные файлы.
    if (!empty($data['attachments'])) {
      foreach ($data['attachments'] as $attachment) {
        $url = urldecode($attachment['data_url']);
        $message = $url;
        switch ($attachment['file_type']) {
          case 'image':
            $message = "";
            $bot->sendImage($chat, $message, $url);
            break;

          case 'audio':
            $message = "";
            $bot->sendAudio($chat, $message, $url);
            break;

          case 'file':
            $message = "";
            $bot->sendFile($chat, $message, $url);
            break;

          default:
            $bot->sendFile($chat, $message, $url);
            \Drupal::logger(__CLASS__ . __FUNCTION__)->warning(
            'UNCNOWN File @type = @url - - - - - @payload', [
              '@type' => $attachment['file_type'],
              '@url' => $url,
              '@payload' => Json::encode($attachment),
            ]);
            break;
        }
      }
    }
  }

  /**
   * Get chat-id from payload.
   */
  private function chatId(array $data) : string {
    $from_conversation = $data['conversation']['custom_attributes']['id'] ?? NULL;
    $from_user = $data['conversation']['meta']['sender']['custom_attributes']['id'] ?? "";
    return $from_conversation ?? $from_user;
  }

  /**
   * Debug staff.
   */
  private function debug(string $payload) : void {
    if (self::DEBUG) {
      \Drupal::logger(__CLASS__ . "::" . __FUNCTION__)->notice(
      '[@type:@bot] Payload: %pl', [
        '@type' => $this->botEntity->getEntityTypeId(),
        '@bot' => $this->botEntity->id(),
        '%pl' => $payload,
      ]);
    }
  }

}
