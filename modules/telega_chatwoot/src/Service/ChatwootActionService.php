<?php

namespace Drupal\telega_chatwoot\Service;

use Drupal\chatwoot\Service\ChatwootService;

/**
 * ChatwootAction service.
 */
class ChatwootActionService {

  const WHATSAPP_INBOX_ID = 3;

  const WELCOME_TEMPLATES = [
    '----',
    '---',
  ];

  // phpcs:ignore
  protected ChatwootService $chatwootService;

  /**
   * Controller constructor.
   */
  public function __construct(ChatwootService $chatwoot_service) {
    $this->chatwootService = $chatwoot_service;
  }

  /**
   * Check start message for welcom templates.
   */
  private function messageIsWelcome(string $content) : bool {
    foreach (self::WELCOME_TEMPLATES as $welcome_template) {
      if (strpos($content, $welcome_template) === 0) {
        return TRUE;
      }
    }
    return FALSE;
  }

  /**
   * Notify about "delivered welcome message".
   */
  public function notifyAboutDeliveredWelcomeMessage(array $chatwoot_event) {
    $event = $chatwoot_event['event'] ?? NULL;
    $inbox_id = $chatwoot_event['inbox']['id'] ?? NULL;
    $message_type = $chatwoot_event['message_type'] ?? NULL;
    $content = $chatwoot_event['content'] ?? NULL;

    $conversation = $chatwoot_event['conversation'] ?? [];
    $messages = $conversation['messages'] ?? [];
    $message = array_shift($messages);
    $message_status = $message['status'] ?? NULL;
    $uid = $conversation['meta']['sender']['identifier'] ?? 0;

    if ($event != 'message_updated') {
      return;
    }
    elseif ($inbox_id != self::WHATSAPP_INBOX_ID) {
      return;
    }
    elseif ($message_type != 'outgoing') {
      return;
    }
    elseif (!$this->messageIsWelcome($content)) {
      return;
    }

    if (!$message_status || !$uid) {
      return;
    }
    // Some staff.
  }

}
