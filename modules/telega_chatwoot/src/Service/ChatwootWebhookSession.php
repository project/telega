<?php

namespace Drupal\telega_chatwoot\Service;

use Drupal\Component\Serialization\Json;
use Drupal\telega_bot\Entity\TelegaBot;

/**
 * Chatwoot service.
 *
 * Дёргается на HookInsert для юзеров/чатов/сообщений.
 */
class ChatwootWebhookSession {

  // phpcs:disable
  const DEBUG = TRUE;
  // phpcs:enable

  /**
   * Constructs a Chatwoot service.
   */
  public function __construct() {

  }

  /**
   * WebHook.
   */
  public function webhook(TelegaBot $bot, array $data) {
    $message_type = $data['message_type'] ?? "";
    if ($message_type == 'outgoing' && !$data['private']) {
      // Сообщение оператора из чатвута отправляем в телегу юзеру.
      if ($chat = $this->chatId($data)) {
        $message = $data['content'];
        $bot_service = \Drupal::service('telega_bot')->init($bot);
        if (is_string($message)) {
          $bot_service->send($chat, $message);
        }
        // Отправляем прикрепленные файлы.
        if (!empty($data['attachments'])) {
          foreach ($data['attachments'] as $attachment) {
            $url = urldecode($attachment['data_url']);
            $message = $url;
            switch ($attachment['file_type']) {
              case 'image':
                $message = "";
                $bot_service->sendImage($chat, $message, $url);
                break;

              case 'audio':
                $message = "";
                $bot_service->sendAudio($chat, $message, $url);
                break;

              case 'file':
                $message = "";
                $bot_service->sendFile($chat, $message, $url);
                break;

              default:
                $bot_service->sendFile($chat, $message, $url);
                \Drupal::logger(__CLASS__ . __FUNCTION__)->warning(
                'UNCNOWN File @type = @url - - - - - @payload', [
                  '@type' => $attachment['file_type'],
                  '@url' => $url,
                  '@payload' => Json::encode($attachment),
                ]);
                break;
            }
          }
        }
      }
    }
  }

  /**
   * Get chat-id from payload.
   */
  private function chatId(array $data) : string {
    $from_conversation = $data['conversation']['custom_attributes']['id'] ?? NULL;
    $from_user = $data['conversation']['meta']['sender']['custom_attributes']['id'] ?? "";
    return $from_conversation ?? $from_user;
  }

  /**
   * Debug staff.
   */
  private function debug(TelegaBot $entity, string $payload) : void {
    if (self::DEBUG) {
      \Drupal::logger(__CLASS__ . "::" . __FUNCTION__)->notice(
      '[@type:@bot] Payload: %pl', [
        '@type' => $entity->getEntityTypeId(),
        '@bot' => $entity->id(),
        '%pl' => $payload,
      ]);
    }
  }

}
