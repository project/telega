<?php

namespace Drupal\telega_chatwoot\Service;

use Drupal\Component\Serialization\Json;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

/**
 * Chatwoot service.
 *
 * Дёргается на HookInsert для юзеров/чатов/сообщений.
 */
class ChatwootService {

  // Установлено на стороне chatwoot.
  const CONTACTS_PAGE_SIZE = 15;

  // phpcs:disable
  private array $opts;
  private string $api;
  private Client $client;
  // phpcs:enable

  /**
   * Constructs a Chatwoot service.
   */
  public function __construct() {
    if ($_SERVER['SHELL'] ?? FALSE) {
      // Is drush.
      return;
    }
    $config = \Drupal::config('telega_chatwoot.settings');
    $id = $config->get('id');
    $tok = $config->get('tok');
    $host = $config->get('host');
    $this->opts = [
      'http_errors' => FALSE,
      'headers' => [
        'Content-type' => "application/json; charset=utf-8",
        'api_access_token' => $tok,
      ],
      'connect_timeout' => 13.14,
      'query' => [],
    ];
    $this->api = "https://$host/api/v1/accounts/$id";
    $this->client = new Client([
      'base_uri' => $this->api,
      'timeout'  => 10.0,
    ]);
  }

  /**
   * Run Debug command: get data from chatwoot.
   */
  public function run() : array {
    $teams = $this->guzzleGet("/teams");
    $agents = $this->guzzleGet("/agents");
    $contacts = $this->guzzleGet("/contacts");
    $data = [
      'teams' => $teams,
      'agents' => $agents,
      'contacts' => $contacts,
    ];
    // $this->addAgentToTeams([]);.
    return $data;
  }

  /**
   * See https://www.chatwoot.com/developers/api/#operation/add-new-agent-to-account.
   */
  public function createAgent(array $options) {
    $data = [
      'name' => $options['name'],
      'email' => $options['name'],
      'role' => 'agent',
      'availability_status' => $options['name'],
      'auto_offline' => $options['name'],
    ];
    $result = $this->guzzlePost("/agents", $data);
    return $result;
  }

  /**
   * Https://www.chatwoot.com/developers/api/#operation/add-new-agent-to-team.
   */
  public function addAgentToTeams(int $teamId, array $options) {
    $data = [
      'user_ids' => $options,
    ];
    $result = $this->guzzlePost("/teams/$teamId/team_members", $data);
    return $result;
  }

  /**
   * See https://www.chatwoot.com/developers/api/#tag/Contacts/operation/contactUpdate.
   */
  public function updateContact(int $id, array $data) {
    $result = $this->guzzlePut("/contacts/$id", $data);
    return $result['payload'] ?? $result;
  }

  /**
   * See https://www.chatwoot.com/developers/api/#operation/create-a-contact.
   */
  public function createContact(array $user) {
    $result = $this->guzzlePost("/contacts", $user);
    return $result['payload']['contact'] ?? $result;
  }

  /**
   * Https://www.chatwoot.com/developers/api/#operation/contactSearch.
   */
  public function searchContact(string $key, bool $full = FALSE) {
    $data = [
      'q' => $key,
    ];
    $contact_id = NULL;
    $result = $this->guzzleGet("/contacts/search", $data);
    if (!empty($result['payload'])) {
      $contact = array_shift($result['payload']);
      $contact_id = $contact['id'];
      if ($full) {
        $api = "/contacts/{$contact_id}/conversations";
        $contact['conversations'] = $this->guzzleGet($api)['payload'];
        return $contact;
      }
    }
    return (string) $contact_id;
  }

  /**
   * Https://www.chatwoot.com/developers/api/#operation/contactSearch.
   *
   * @depreceted
   *   Лучше написать нормальный filter.
   */
  public function superSearchContact(
    string $search_string,
    string $field = 'identifier',
    int $page = 1
  ) :? array {
    $data = [
      'q' => $search_string,
      'page' => $page,
    ];
    $result = $this->guzzleGet("/contacts/search", $data);
    if (empty($result['meta']['count'])) {
      return NULL;
    }
    $contacts = array_filter(
    $result['payload'], fn($contact) => $contact[$field] == $search_string
    );
    if (empty($contacts)) {
      $contact_pages = ceil($result['meta']['count'] / self::CONTACTS_PAGE_SIZE);
      if ($contact_pages < $page + 1) {
        return NULL;
      }
      return $this->superSearchContact($search_string, $field, $page + 1);
    }
    $contact = array_shift($contacts);
    $contact_id = $contact['id'];
    $conversations = $this->guzzleGet(
    "/contacts/{$contact_id}/conversations"
    );
    $contact['conversations'] = $conversations['payload'] ?? [];
    $contact['$page'] = $page;
    return $contact;
  }

  /**
   * See https://www.chatwoot.com/developers/api/#operation/newConversation.
   */
  public function createConversation(array $conversation) {
    $result = $this->guzzlePost("/conversations", $conversation);
    return $result;
  }

  /**
   * See https://www.chatwoot.com/developers/api/#operation/create-a-new-message-in-a-conversation.
   */
  public function createMessage(array $options) {
    // Сейчас мы ищем в чатвуте контакт по идентифиаеру вида tg_437434
    // Хотя скорее всего стоит в том числе искать по
    // - телефону (существующее поле)
    // - tg-логину добавить такое поле
    // - tg-идентифаеру (если существующий контакт написал в бота и объединили)
    $contact = $this->filterContactByTgId($options['user']);
    if (empty($contact)) {
      \Drupal::logger("Telega-chatwoot " . __FUNCTION__)->error(
        'Cannot find chatwoot user for tg - id: @tg', [
          '@tg' => $options['user'],
        ]);
      return FALSE;
    }

    $conversations = $this->getConversationByContact($contact, $options['inbox_id']);
    if (empty($conversations)) {
      \Drupal::logger("Telega-chatwoot " . __FUNCTION__)->error(
        'Cannot find conversations for tg-id: @tg, chw-id: @chw', [
          '@tg' => $options['user'],
          '@chw' => $contact,
        ]);
      // Create!
      $conversation = [
        "status" => "open",
        'source_id' => "tg_{$options['user']}_{$options['inbox_id']}",
        'contact_id' => $contact,
        'inbox_id' => $options['inbox_id'],
        'custom_attributes' => [
          'created' => time(),
          'id' => $options['user'] ?? "",
          'bot' => $options['bot'] ?? "",
          'name' => $options['userTitle'] ?? "",
        ],
      ];
      $this->createConversation($conversation);
      $conversations = $this->getConversationByContact($contact, $options['inbox_id']);
    }
    if (!empty($conversations)) {
      $conversation_id = array_key_first($conversations);
      $text = $options['user'] < 0 ? "{$options['userTitle']}: {$options['text']}" : $options['text'];
      $data = [
        "content" => $text,
      // "outgoing | incoming".
        "message_type" => $options['message_type'] ?? "incoming",
        "private" => $options['private'] ?? FALSE,
      // "content_attributes" => [],
      ];
      $api = "/conversations/{$conversation_id}/messages";
      return $this->guzzlePost($api, $data);
    }
    return FALSE;
  }

  /**
   * Create Sysntem Message.
   */
  public function createSystemMessage(string $text, string $chat, string $inbox) {
    $contact = $this->filterContactByTgId($chat);
    if ($contact) {
      $conversations = $this->getConversationByContact($contact, $inbox);
      if (!empty($conversations)) {
        $conversation_id = array_key_first($conversations);
        $data = [
          "content" => $text,
        // "outgoing | incoming".
          "message_type" => "outgoing",
          "private" => TRUE,
        // "content_attributes" => [],
        ];
        $api = "/conversations/{$conversation_id}/messages";
        $result = $this->guzzlePost($api, $data);
        return $result;
      }
      else {
        \Drupal::logger("Telega-chatwoot " . __FUNCTION__)->error(
          'Cannot find conversations for tg - id: @tg, chw - id: @chw', [
            '@tg' => $chat,
            '@chw' => $contact,
          ]);
      }
    }
    else {
      \Drupal::logger("Telega-chatwoot " . __FUNCTION__)->error(
        'Cannot find chatwoot user for tg - id: @tg', [
          '@tg' => $chat,
        ]);
    }
    return FALSE;
  }

  /**
   * Https://www.chatwoot.com/developers/api/#tag/Contacts/operation/contactFilter.
   */
  private function filterContactByTgId(string $id) :? string {
    // Сейчас мы ищем в чатвуте контакт по идентифиаеру вида tg_437434
    // Хотя скорее всего стоит в том числе искать по телефону или tg-логину.
    // 'attribute_key' => "identifier",'values' => [$id],.
    $data = [
      [
        'attribute_key' => 'identifier',
        'attribute_model' => "standard",
        'custom_attribute_type' => "",
        'filter_operator' => 'equal_to',
        'values' => ["tg_$id"],
      ],
    ];
    $contact_id = NULL;
    $result = $this->guzzlePost("/contacts/filter", [
      'payload' => $data,
    ]);
    if (!empty($result['payload'])) {
      $contact = array_shift($result['payload']);
      $contact_id = $contact['id'];
    }
    else {
      \Drupal::logger("Telega-chatwoot " . __FUNCTION__)->warning(
        'Miss: @json', [
          '@json' => Json::encode($data),
        ]);
    }
    return $contact_id;
  }

  /**
   * Find Conversation.
   */
  private function getConversationByContact(string $contact, string $inbox) : array {
    $request = $this->guzzleGet("/contacts/{$contact}/conversations");
    $conversations = [];
    foreach ($request['payload'] ?? [] as $conversation) {
      if ($conversation['inbox_id'] == $inbox) {
        $key = $conversation['id'];
        $conversations[$key] = $conversation;
      }
    }
    return $conversations;
  }

  /**
   * Https://www.chatwoot.com/developers/api/#operation/contactSearch.
   */
  private function searchContactByTgId(string $key, bool $full = FALSE) {
    $data = [
      'q' => "tg_$key",
    ];
    $contact_id = NULL;
    $result = $this->guzzleGet("/contacts/search", $data);
    if (!empty($result['payload'])) {
      $contact = array_shift($result['payload']);
      $contact_id = $contact['id'];
      if ($full) {
        $api = "/contacts/{$contact_id}/conversations";
        $contact['conversations'] = $this->guzzleGet($api)['payload'];
        return $contact;
      }
    }
    return (string) $contact_id;
  }

  /**
   * Find Inbox, deprecated.
   */
  private function messageInbox(int $bot, array $contact) : int {
    // 1. Ищем нужный инбокс по урлу вебхука:
    $inbox = 0;
    if (!empty($contact['contact_inboxes'])) {
      foreach ($contact['contact_inboxes'] as $inbox_item) {
        $hook = $inbox_item['inbox']['webhook_url'];
        if (strpos($hook, 'telega / chatwoot / webhook')) {
          $url = explode("/", $hook);
          if ($bot == end($url)) {
            $inbox = $inbox_item['inbox']['id'];
          }
        }
      }
    }
    return $inbox;
  }

  /**
   * See https://www.chatwoot.com/developers/api/#tag/Messages-API/operation/list-all-converation-messages.
   */
  public function listMessagesConversation($inboxId, $contactId, $conversationId) {
    // https://app.chatwoot.com/public/api/v1/inboxes/{inbox_identifier}/contacts/{contact_identifier}/conversations/{conversation_id}/messages
    $url = "/inboxes/$inboxId/contacts/$contactId/conversations/$conversationId/messages";
    $result = $this->guzzleGet($url);
    return $result;
  }

  /**
   * See https://www.chatwoot.com/developers/api/#tag/Webhooks/operation/list-all-webhooks.
   */
  public function listAllWebhooks() : array {
    $url = "/webhooks";
    $result = $this->guzzleGet($url);
    return $result;
  }

  /**
   * See https://www.chatwoot.com/developers/api/#tag/Webhooks/operation/update-a-webhook.
   */
  public function updateWebhook(int $webhook_id, array $data) : array {
    $url = "/webhooks/$webhook_id";
    $result = $this->guzzlePatch($url, $data);
    return $result;
  }

  /**
   * GUZZLE: Get data.
   */
  private function guzzleGet($url, $params = []) {
    $options = $this->opts;
    $options['json'] = $params;
    try {
      $response = $this->client->get($this->api . "{$url}", $options);
      $code = $response->getStatusCode();
      $body = $response->getBody()->getContents();
      if (in_array(substr($body, 0, 1), ['{', '['])) {
        $body = Json::decode($body);
      }
      if ($code == 200) {
        return $body;
      }
      if ($code == 201) {
        return $body ?: "OK";
      }
      return [
        'header' => $response->getHeaders(),
        'code' => $code,
        'body' => $body,
        'uri' => $url,
      ];
    }
    catch (RequestException $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }
    return "FALSE";
  }

  /**
   * Guzzle Post.
   */
  private function guzzlePost($url, $params = []) {
    $options = $this->opts;
    $options['json'] = $params;
    // $options['debug'] = TRUE;
    try {
      $response = $this->client->post($this->api . "{$url}", $options);
      $code = $response->getStatusCode();
      $body = $response->getBody()->getContents();
      if (in_array(substr($body, 0, 1), ['{', '['])) {
        $body = Json::decode($body);
      }
      if ($code == 200) {
        return $body;
      }
      if ($code == 201) {
        return $body ?: "201 Created, Success";
      }
      if ($code == 204) {
        return $body ?: "204 No Content, Success";
      }
      if ($code == 500) {
        // \Drupal::messenger()->addError('500');
      }
      return [
        'header' => $response->getHeaders(),
        'code' => $code,
        'body' => $body,
        'uri' => $url,
      ];
    }
    catch (RequestException $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }
    return "FALSE";
  }

  /**
   * Guzzle put.
   */
  private function guzzlePut(string $url, array $data) :? array {
    $options = $this->opts;
    $options['json'] = $data;
    try {
      $response = $this->client->put($this->api . $url, $options);
      $code = $response->getStatusCode();
      $body = $response->getBody()->getContents();
      if (in_array(substr($body, 0, 1), ['{', '['])) {
        $body = Json::decode($body);
      }
      if ($code == 200) {
        return $body;
      }
      if ($code == 201) {
        return $body ?: "201 Created, Success";
      }
      if ($code == 204) {
        return $body ?: "204 No Content, Success";
      }
      return [
        'header' => $response->getHeaders(),
        'code' => $code,
        'body' => $body,
        'uri' => $url,
      ];
    }
    catch (RequestException $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }
    return NULL;
  }

  /**
   * Guzzle patch.
   */
  private function guzzlePatch(string $url, array $data) :? array {
    $options = $this->opts;
    $options['json'] = $data;
    try {
      $response = $this->client->put($this->api . $url, $options);
      $code = $response->getStatusCode();
      $body = $response->getBody()->getContents();
      if (in_array(substr($body, 0, 1), ['{', '['])) {
        $body = Json::decode($body);
      }
      if ($code == 200) {
        return $body;
      }
      if ($code == 201) {
        return $body ?: "201 Created, Success";
      }
      if ($code == 204) {
        return $body ?: "204 No Content, Success";
      }
      return [
        'header' => $response->getHeaders(),
        'code' => $code,
        'body' => $body,
        'uri' => $url,
      ];
    }
    catch (RequestException $e) {
      \Drupal::messenger()->addError($e->getMessage());
    }
    return NULL;
  }

}
