<?php

namespace Drupal\telega_chatwoot\Controller;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Drupal\telega_bot\Entity\TelegaBot;
use Drupal\telega_session\Entity\TelegaSession;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Returns responses for guzzle routes.
 */
class WebhookController extends ControllerBase {

  const DEBUG = FALSE;

  /**
   * Build BOT-webhook responce.
   */
  public function webhookBot(Request $request, TelegaBot $bot) {
    $payload = $request->getContent();
    // $this->debug($bot, $payload);
    $data = Json::decode($payload);
    if (is_array($data)) {
      // @todo когда-то это работало на сендере.
      // \Drupal::service('chatwoot.action')
      // ->notifyAboutDeliveredWelcomeMessage($data);
      \Drupal::service('telega_chatwoot_bot_webhook')->webhook($bot, $data);
      return new JsonResponse(["status" => "ok"]);
    }
    return new JsonResponse(["status" => "fail"]);
  }

  /**
   * Build Session-webhook responce.
   */
  public function webhookSession(Request $request, TelegaSession $session) {
    $chat = "";
    $payload = $request->getContent();
    $this->debug($session, $payload);

    $data = Json::decode($payload);
    $message_type = $data['message_type'] ?? "";
    if ($message_type == 'outgoing' && !$data['private']) {
      // Сообщение оператора из чатвута отправляем в телегу юзеру.
      if ($chat = $this->chatId($data)) {
        $message = $data['content'];
        $session_service = \Drupal::service('telega_session')->init($session);
        $session_service = $session_service->sendMessage($message);
      }
    }
    return new JsonResponse(["status" => "ok", "chat" => $chat]);
  }

  /**
   * Debug staff.
   */
  private function debug(TelegaBot|TelegaSession $entity, string $payload) : void {
    if (self::DEBUG) {
      \Drupal::logger(__CLASS__ . "::" . __FUNCTION__)->notice(
      '[@type:@bot] Payload: %pl', [
        '@type' => $entity->getEntityTypeId(),
        '@bot' => $entity->id(),
        '%pl' => $payload,
      ]);
    }
  }

}
