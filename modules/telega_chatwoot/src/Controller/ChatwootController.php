<?php

namespace Drupal\telega_chatwoot\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Returns responses for guzzle routes.
 */
class ChatwootController extends ControllerBase {

  /**
   * Builds the response for debug.
   */
  public function build() {
    $chatwoot = \Drupal::service('telega_chatwoot')->run();
    $build['content'] = [
      '#type' => 'item',
      '#markup' => $this->t('It works!'),
    ];
    return $build;
  }

}
