<?php

namespace Drupal\telega_chatwoot\Form;

/**
 * @file
 * Contains Drupal\telega_chatwoot\Form\Settings.
 */

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements the Chatwoot form controller.
 */
class Settings extends ConfigFormBase {

  /**
   * Build.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('telega_chatwoot.settings');
    $form['general'] = [
      '#type' => 'details',
      '#title' => $this->t('General settings'),
      '#open' => TRUE,
    ];
    $form['general']['enable'] = [
      '#title' => $this->t('Enable chatwoot integration'),
      '#type' => 'checkbox',
      '#default_value' => $config->get('enable'),
    ];
    $form['general']['host'] = [
      '#title' => $this->t('Chatwoot host'),
      '#type' => 'textfield',
      '#default_value' => $config->get('host'),
    ];
    $form['general']['tok'] = [
      '#title' => $this->t('API-token'),
      '#default_value' => $config->get('tok'),
      '#type' => 'textfield',
    ];
    $form['general']['id'] = [
      '#title' => $this->t('Installation ID'),
      '#default_value' => $config->get('id'),
      '#type' => 'textfield',
    ];
    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'telega_chatwoot_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['telega_chatwoot.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('telega_chatwoot.settings');
    $config
      ->set('enable', $form_state->getValue('enable'))
      ->set('host', $form_state->getValue('host'))
      ->set('tok', $form_state->getValue('tok'))
      ->set('id', $form_state->getValue('id'));
    $config->save();
    parent::submitForm($form, $form_state);
  }

}
