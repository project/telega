<?php

namespace Drupal\telega_chatwoot\Hook;

/**
 * @file
 * Contains \Drupal\telega_chatwoot\Hook\*View.
 */

/**
 * Hook User View.
 */
class TelegaUserView {

  /**
   * Hook.
   */
  public static function hook(&$build, $entity, $view_mode) {
    if ($view_mode == 'full') {
      TelegaUserInsert::hook($entity);
    }
  }

}
