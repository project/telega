<?php

namespace Drupal\telega_chatwoot\Hook;

use Drupal\telega_chat\Entity\TelegaChat;

/**
 * @file
 * Contains \Drupal\telega_chatwoot\Hook\*Insert.
 */

/**
 * Hook Chat Insert.
 */
class TelegaChatInsert {

  /**
   * Hook.
   */
  public static function hook(TelegaChat $entity) {
    $key = $entity->key->value;
    $bot = $entity->bot_id->entity;
    $bot_id = $bot->id();
    $user = [
      'name' => $entity->title->value,
      'identifier' => "tg_$key",
      'inbox_id' => $bot->getYamlParam('chatwoot/inbox'),
      'custom_attributes' => [
        'created' => $entity->created->value,
        'id' => $entity->key->value,
        'bot' => $bot->id(),
        'name' => $entity->name->value,
        'first_name' => $entity->first_name->value,
        'last_name' => $entity->last_name->value,
      ],
    ];
    \Drupal::service('telega_chatwoot')->createContact($user);
    $contact = \Drupal::service('telega_chatwoot')->searchContact("tg_$key");
    // Chat conversation.
    $conversation = [
      "status" => "open",
      'source_id' => "tg_{$key}_{$bot_id}",
      'contact_id' => $contact,
      'inbox_id' => $bot->getYamlParam('chatwoot/inbox'),
      'custom_attributes' => [
        'created' => $entity->created->value,
        'type' => $entity->type->value,
        'id' => $key,
        'bot' => $bot->id(),
        'name' => $entity->name->value,
        'first_name' => $entity->first_name->value,
        'last_name' => $entity->last_name->value,
      ],
    ];
    \Drupal::service('telega_chatwoot')->createConversation($conversation);
  }

}
