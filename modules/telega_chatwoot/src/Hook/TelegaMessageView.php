<?php

namespace Drupal\telega_chatwoot\Hook;

/**
 * @file
 * Contains \Drupal\chatwoot\Hook\*View.
 */

/**
 * Hook Message View.
 */
class TelegaMessageView {

  /**
   * Hook.
   */
  public static function hook(&$build, $entity, $view_mode) {
    if ($view_mode == 'full') {
      // TelegaMessageInsert::hook($entity);
    }
  }

}
