<?php

namespace Drupal\telega_chatwoot\Hook;

/**
 * @file
 * Contains \Drupal\telega_chatwoot\Hook\*View.
 */

/**
 * Hook Chat View.
 */
class TelegaChatView {

  /**
   * Hook.
   */
  public static function hook(&$build, $entity, $view_mode) {
    if ($view_mode == 'full') {
      // TelegaChatInsert::hook($entity);
    }
  }

}
