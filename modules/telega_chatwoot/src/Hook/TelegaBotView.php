<?php

namespace Drupal\telega_chatwoot\Hook;

/**
 * @file
 * Contains \Drupal\telega_chatwoot\Hook\*View.
 */

/**
 * Hook Chat View.
 */
class TelegaBotView {

  /**
   * Hook.
   */
  public static function hook(&$build, $entity, $view_mode) {
    if ($view_mode == 'full') {
      $url = \Drupal::request()->getSchemeAndHttpHost();
      $id = $entity->id();
      $build['chatwoot'] = [
        '#type' => 'details',
        '#title' => t('Chatwoot'),
        '#open' => TRUE,
        '#weight' => -10,
        'info' => [
          '#markup' => "1) Webhook url = `$url/telega/chatwoot/webhook/bot/$id`",
        ],
        'edit' => [
          '#prefix' => '<br>',
          '#markup' => "2) Do not forget set Chatwoot Inbox in YAML field",
        ],
      ];
    }
  }

}
