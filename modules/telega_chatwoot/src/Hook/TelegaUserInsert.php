<?php

namespace Drupal\telega_chatwoot\Hook;

use Drupal\telega_user\Entity\TelegaUser;

/**
 * @file
 * Contains \Drupal\telega_chatwoot\Hook\*Insert.
 */

/**
 * Hook User Insert.
 */
class TelegaUserInsert {

  /**
   * Hook.
   */
  public static function hook(TelegaUser $entity) {
    $key = $entity->key->value;
    // $bot = $entity->bot_id->entity;
    $user = [
      'name' => $entity->title->value,
      'identifier' => "tg_$key",
      // 'inbox_id' => $bot->getYamlParam('chatwoot/inbox'),
      'custom_attributes' => [
        'created' => $entity->created->value,
        'id' => $entity->key->value,
        'name' => $entity->name->value,
        'first_name' => $entity->first_name->value,
        'last_name' => $entity->last_name->value,
      ],
    ];
    \Drupal::service('telega_chatwoot')->createContact($user);
  }

}
