<?php

namespace Drupal\telega_chatwoot\Hook;

use Drupal\telega_message\Entity\TelegaMessage;

/**
 * @file
 * Contains \Drupal\telega_chatwoot\Hook\*Insert.
 */

/**
 * Hook Message Insert.
 */
class TelegaMessageInsert {

  /**
   * Hook.
   */
  public static function hook(TelegaMessage $telega_message) {
    if (self::isMessageFromCallbackQuery($telega_message)) {
      $text = "";
    }
    $text = self::getMessageText($telega_message);
    $key = $telega_message->key->value;
    $userKey = $telega_message->user_id->entity->key->value;
    $chatKey = $telega_message->chat_id->entity->key->value;
    $bot = $telega_message->bot_id->entity;
    $msg = [
      'key' => $telega_message->key->value,
      'title' => $telega_message->title->value,
      'inbox_id' => $bot->getYamlParam('chatwoot/inbox'),
      'text' => $text,
      'user' => $chatKey,
      'userTitle' => "{$telega_message->user_id->entity->getTitle()} ({$userKey})",
      // @todo понять нужно ли это с новой структурой.
      'bot' => 'telega_' . $bot->id(),
      'identifier' => "tg_$key",
    ];
    if ($telega_message->getTitle() == 'callback_query') {
      $msg['message_type'] = 'outgoing';
      $msg['private'] = TRUE;
    }
    \Drupal::moduleHandler()->alter('telega_chatwoot_create_message', $msg);
    if ($msg) {
      \Drupal::service('telega_chatwoot')->createMessage($msg);
    }
  }

  /**
   * Это колбэк от нажатия кнопки.
   */
  private static function isMessageFromCallbackQuery(TelegaMessage $telega_message) : bool {
    return $telega_message->title->value === 'callback_query';
  }

  /**
   * Сформировать текст сообщения.
   */
  private static function getMessageText(TelegaMessage $telega_message) :? string {
    $message_text = trim($telega_message->text->value ?? "");
    $reply_to_text = self::getReplyToMessageText($telega_message);
    $attachment_link = self::getAttachmentLink($telega_message);
    if ($yaml = $telega_message->yaml->value) {
      return $yaml;
    }
    if (!$reply_to_text && !$attachment_link) {
      return $message_text;
    }
    elseif ($reply_to_text && $attachment_link) {
      return sprintf(
        '```%s```%c%s%c%s',
        $reply_to_text, 10, $message_text, 10, $attachment_link
      );
    }
    elseif ($reply_to_text) {
      return sprintf(
        '```%s```%c%s', $reply_to_text, 10, $message_text
      );
    }
    elseif ($attachment_link) {
      return sprintf('%s%c%s', $message_text, 10, $attachment_link);
    }
    return NULL;
  }

  /**
   * Получить ссылку на вложение.
   */
  private static function getAttachmentLink(TelegaMessage $telega_message) :? string {
    if ($telega_message->attachment->isEmpty()) {
      return NULL;
    }
    $file = $telega_message->attachment->entity;
    return $file->createFileUrl(FALSE);
  }

  /**
   * Получить текст сообщения на которое отвечали.
   */
  private static function getReplyToMessageText(TelegaMessage $telega_message) :? string {
    if ($telega_message->reply_to_message_id->isEmpty()) {
      return NULL;
    }
    $reply_to_message = $telega_message->reply_to_message_id->entity;
    $message_text = trim($reply_to_message->text->value);
    $attachment_link = self::getAttachmentLink($reply_to_message);
    if (!$attachment_link) {
      return $message_text;
    }
    return sprintf('%s%c%s', $message_text, 10, $attachment_link);
  }

}
