# Изменение сообщения перед отправкой в чатвут

- Этим хуком можно изменить структуру или отменить отправку.

```php
<?php

/**
 * @file
 * Primary module hooks for module.
 */

/**
 * Implements hook_telega_chatwoot_create_message_alter().
 */
function HOOK_telega_chatwoot_create_message_alter(array &$message) {
  // Не отправлять сообщения, выглядящие как команды в телеге в чатвут.
  if (strpos($message['text'], '/') === 0) {
    $message = NULL;
  }
}

```
