<?php

namespace Drupal\telega_chat\Controller;

/**
 * @file
 * Contains \Drupal\telega_chat\Controller\ChatPage.
 */

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller ChatPage.
 */
class ChatPage extends ControllerBase {

  /**
   * Render Page.
   */
  public function page(Request $request) {
    return [
      'text' => ['#markup' => "<pre>==</pre>"],
    ];
  }

}
