<?php

namespace Drupal\telega_chat\Entity;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\telega\Utility\ContentEntity;
use Drupal\telega\Utility\FieldDefinition;

/**
 * Defines the chat entity class.
 *
 * @ContentEntityType(
 *   id = "telega_chat",
 *   label = @Translation("Chat"),
 *   label_collection = @Translation("Chats"),
 *   handlers = {
 *     "view_builder" = "Drupal\telega_chat\Entity\TelegaChatViewBuilder",
 *     "list_builder" = "Drupal\telega_chat\Entity\TelegaChatListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\telega_chat\Form\TelegaChatForm",
 *       "edit" = "Drupal\telega_chat\Form\TelegaChatForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "telega_chat",
 *   admin_permission = "administer chat",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/telega/chat/add",
 *     "canonical" = "/telega/chat/{telega_chat}",
 *     "edit-form" = "/telega/chat/{telega_chat}/edit",
 *     "delete-form" = "/telega/chat/{telega_chat}/delete",
 *     "collection" = "/telega/chat"
 *   },
 *   field_ui_base_route = "entity.telega_chat.settings"
 * )
 */
class TelegaChat extends ContentEntity implements TelegaChatInterface {

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['title'] = FieldDefinition::string('Title');
    $fields['status'] = FieldDefinition::status();
    $fields['uid'] = FieldDefinition::uid();
    $fields['created'] = FieldDefinition::created();
    $fields['changed'] = FieldDefinition::changed();
    // Custom.
    $fields['type'] = FieldDefinition::list('Type', [
      'private' => "Private",
      'group' => "Group",
      'supergroup' => "Supergroup",
      'channel' => "Channel",
    ]);
    $fields['key'] = FieldDefinition::bigint('Key');
    $fields['bot_id'] = FieldDefinition::entity('Bot', 'telega_bot');
    $fields['name'] = FieldDefinition::string('Name');
    $fields['first_name'] = FieldDefinition::string('First Name');
    $fields['last_name'] = FieldDefinition::string('Last Name');
    $fields['all_admins'] = FieldDefinition::bool('All admins');
    $fields['key_old'] = FieldDefinition::bigint('Old Key');
    $fields['yaml'] = FieldDefinition::long('Yaml');
    $fields['data'] = FieldDefinition::map('Extra');
    return $fields;
  }

}
