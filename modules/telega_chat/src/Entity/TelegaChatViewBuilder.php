<?php

namespace Drupal\telega_chat\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Provides a view controller for TelegaChat entity type.
 */
class TelegaChatViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getBuildDefaults(EntityInterface $entity, $view_mode) {
    $build = parent::getBuildDefaults($entity, $view_mode);
    if ($view_mode == 'full') {
      $f = 'Drupal\telega_chat\Form';
      $from = \Drupal::formBuilder()->getForm("$f\ChatForm", $entity);
      $build['form'] = $from;
      $build['last'] = [
        'messages' => \Drupal::service('telega_message')->renderLast($entity),
      ];
    }
    return $build;
  }

}
