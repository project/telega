<?php

namespace Drupal\telega_chat\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a chat entity type.
 */
interface TelegaChatInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the chat title.
   *
   * @return string
   *   Title of the chat.
   */
  public function getTitle();

  /**
   * Sets the chat title.
   *
   * @param string $title
   *   The chat title.
   *
   * @return \Drupal\telega_chat\TelegaChatInterface
   *   The called chat entity.
   */
  public function setTitle($title);

  /**
   * Gets the chat creation timestamp.
   *
   * @return int
   *   Creation timestamp of the chat.
   */
  public function getCreatedTime();

  /**
   * Sets the chat creation timestamp.
   *
   * @param int $timestamp
   *   The chat creation timestamp.
   *
   * @return \Drupal\telega_chat\TelegaChatInterface
   *   The called chat entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the chat status.
   *
   * @return bool
   *   TRUE if the chat is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the chat status.
   *
   * @param bool $status
   *   TRUE to enable this chat, FALSE to disable.
   *
   * @return \Drupal\telega_chat\TelegaChatInterface
   *   The called chat entity.
   */
  public function setStatus($status);

}
