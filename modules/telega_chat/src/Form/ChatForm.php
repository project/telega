<?php

namespace Drupal\telega_chat\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\telega\Utility\AjaxResult;

/**
 * Implements the form controller.
 */
class ChatForm extends FormBase {

  /**
   * Ajax Wrapper.
   *
   * @var string
   */
  private $wrapper = 'telega-chat-form-wrap';

  /**
   * AJAX Version.
   */
  public function ajaxSend(array &$form, $form_state) {
    $entity = $form_state->entity;
    $name = $entity->title->value;
    $chat = $entity->key->value;
    $text = $form_state->getValue('text');
    $otvet = "send $name [$chat]:\n";
    $otvet .= "Text: $text\n";
    $bot = \Drupal::service('telega_bot')->init($entity->bot_id->entity);
    $send = $bot->send($chat, $text);
    if ($send['ok']) {
      $otvet .= "Message id: {$send['result']['message_id']}";
    }
    else {
      $otvet .= "error";
    }
    return AjaxResult::ajax($this->wrapper, $otvet);
  }

  /**
   * AJAX Version.
   */
  public function ajaxSayHello(array &$form, $form_state) {
    $entity = $form_state->entity;
    $name = $entity->title->value;
    $chat = $entity->key->value;
    $otvet = "send $name [$chat]:\n";
    $bot = \Drupal::service('telega_bot')->init($entity->bot_id->entity);
    $send = $bot->send($chat, "Hello there");
    if ($send['ok']) {
      $otvet .= "Message id: {$send['result']['message_id']}";
    }
    else {
      $otvet .= "error";
    }
    return AjaxResult::ajax($this->wrapper, $otvet);
  }

  /**
   * AJAX Version.
   */
  public function ajaxInlineKb(array &$form, $form_state) {
    $entity = $form_state->entity;
    $name = $entity->title->value;
    $chat = $entity->key->value;
    $otvet = "send $name [$chat]:\n";
    $bot = \Drupal::service('telega_bot')->init($entity->bot_id->entity);
    $send = $bot->sendInlineKeyboard($chat, "Inline Kb");
    if ($send['ok']) {
      $otvet .= "Message id: {$send['result']['message_id']}";
    }
    else {
      $otvet .= "error";
    }
    return AjaxResult::ajax($this->wrapper, $otvet);
  }

  /**
   * Build the simple form.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $extra = NULL) {
    $wrap = $this->wrapper;
    $form_state->entity = $extra;
    $form_state->setCached(FALSE);
    if ($extra->key->value > 0) {
      $form['main'] = [
        '#type' => 'details',
        '#title' => $this->t('Send Message'),
        '#open' => TRUE,
        'text' => [
          '#type' => 'textarea',
          '#title' => $this->t('Message'),
          '#rows' => 2,
        ],
        'send' => AjaxResult::button('::ajaxSend', 'Send'),
        'keyboard' => AjaxResult::button('::ajaxInlineKb', 'KeyBoard'),
        'hello' => AjaxResult::button('::ajaxSayHello', 'Say Hello'),
      ];
    }
    else {
      $form['group'] = [
        '#type' => 'details',
        '#title' => $this->t('Group Actions'),
        '#open' => TRUE,
        'text' => [
          '#type' => 'textarea',
          '#title' => $this->t('Message'),
          '#rows' => 2,
        ],
        'send' => AjaxResult::button('::ajaxSend', 'Send'),
        'hello' => AjaxResult::button('::ajaxSayHello', 'Say Hello'),
        'keyboard' => AjaxResult::button('::ajaxInlineKb', 'KeyBoard'),
      ];
    }
    $form['output'] = [
      '#suffix' => "<div id='$wrap'><pre>></pre></div>",
    ];
    return $form;
  }

  /**
   * Implements a form submit handler.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'telega_chat_form';
  }

}
