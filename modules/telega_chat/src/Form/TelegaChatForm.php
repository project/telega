<?php

namespace Drupal\telega_chat\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the chat entity edit forms.
 */
class TelegaChatForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + [
      'link' => \Drupal::service('renderer')->render($link),
    ];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New chat %label has been created.', $message_arguments));
      $this->logger('telega_chat')->notice('Created new chat %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The chat %label has been updated.', $message_arguments));
      $this->logger('telega_chat')->notice('Updated new chat %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.telega_chat.canonical', ['telega_chat' => $entity->id()]);
  }

}
