<?php

namespace Longman\TelegramBot\Commands\SystemCommands;

use Drupal\telega\Utility\SystemCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;

/**
 * Edited message command.
 *
 * Gets executed when a user message is edited.
 */
class EditedmessageCommand extends SystemCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'editedmessage';
    $this->description = 'Handle edited message';
    $this->version = '1.2.0';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    // Get the edited message.
    $edited_message = $this->getEditedMessage();

    return parent::execute();
  }

}
