<?php

namespace Longman\TelegramBot\Commands\SystemCommands;

use Drupal\telega\Utility\SystemCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;

/**
 * Edited channel post command.
 *
 * Gets executed when a post in a channel is edited.
 */
class EditedchannelpostCommand extends SystemCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'editedchannelpost';
    $this->description = 'Handle edited channel post';
    $this->version = '1.1.0';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    // Get the edited channel post.
    $edited_channel_post = $this->getEditedChannelPost();

    return parent::execute();
  }

}
