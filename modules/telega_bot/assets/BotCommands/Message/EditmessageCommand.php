<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Drupal\telega\Utility\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;

/**
 * User "/editmessage" command.
 *
 * Command to edit a message sent by the bot.
 */
class EditmessageCommand extends UserCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'editmessage';
    $this->description = 'Edit a message sent by the bot';
    $this->version = '1.1.0';
    $this->usage = '/editmessage';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    $message          = $this->getMessage();
    $chat_id          = $message->getChat()->getId();
    $reply_to_message = $message->getReplyToMessage();
    $text             = $message->getText(TRUE);

    if ($reply_to_message && $message_to_edit = $reply_to_message->getMessageId()) {
      // Try to edit the selected message.
      $result = Request::editMessageText([
        'chat_id'    => $chat_id,
        'message_id' => $message_to_edit,
        'text'       => $text ?: 'Edited message',
      ]);

      // If successful, delete this editing reply message.
      if ($result->isOk()) {
        Request::deleteMessage([
          'chat_id'    => $chat_id,
          'message_id' => $message->getMessageId(),
        ]);
      }

      return $result;
    }
    $reply = sprintf("Reply to any bots' message and use /%s <your text> to edit it.", $this->getName());

    return $this->replyToChat($reply);
  }

}
