<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Drupal\telega\Utility\UserCommand;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;

/**
 * User "/cancel" command.
 *
 * This command cancels the currently active conversation and
 * returns a message to let the user know which conversation it was.
 *
 * If no conversation is active, the returned message says so.
 */
class CancelCommand extends UserCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'cancel';
    $this->usage = '/cancel';
    $this->private_only = FALSE;
    $this->description = 'Cancel the currently active conversation';
    $this->version = '0.3.0';
  }

  /**
   * Main command execution if no DB connection is available.
   *
   * @throws \Longman\TelegramBot\Exception\TelegramException
   */
  public function executeNoDb(): ServerResponse {
    return $this->removeKeyboard('Nothing to cancel.');
  }

  /**
   * Remove the keyboard and output a text.
   *
   * @throws \Longman\TelegramBot\Exception\TelegramException
   */
  private function removeKeyboard(string $text): ServerResponse {
    return $this->replyToChat($text, [
      'reply_markup' => Keyboard::remove(['selective' => TRUE]),
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {

    // Cancel current conversation if any.
    $this->setVal("state", "");
    $text = 'No active conversation!';
    $conversation = $this->getVal("conversation");
    if ($conversation) {
      $this->stopConversation();
      $text = "Conversation /$conversation cancelled!";
    }
    return $this->removeKeyboard($text);
  }

}
