<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Drupal\telega\Utility\UserCommand;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\KeyboardButton;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;

/**
 * User "/survey" command.
 *
 * Example of the Conversation functionality in form of a simple survey.
 */
class SurveyCommand extends UserCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'survey';
    $this->usage = '/survey';
    $this->description = 'Survey for bot users';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    $message = $this->getMessage();
    $chat = $message->getChat();
    $user = $message->getFrom();
    $text = trim($message->getText(TRUE));

    // Preparing response.
    $result = Request::emptyResponse();
    $data = [
      'chat_id' => $chat->getId(),
      'parse_mode' => 'markdown',
      'reply_markup' => Keyboard::remove(['selective' => TRUE]),
    ];
    if ($chat->isGroupChat() || $chat->isSuperGroup()) {
      // Force reply is applied by default so it can work with privacy on.
      $data['reply_markup'] = Keyboard::forceReply(['selective' => TRUE]);
    }

    // Conversation start.
    $conversation = $this->getVal("conversation");
    if ($conversation != $this->getName()) {
      $this->startConversation();
      $this->setVal("state", "0");
    }
    $state = $this->getVal("state") ?? 0;

    // State machine.
    switch ($state) {
      case 0:
        $this->stepStart('name', $state);
        // cmd.
        if ($text === '') {
          $data['text'] = 'Type your name:';
          $result = Request::sendMessage($data);
          break;
        }
        $text = $this->stepStop($text);
        // No break!
      case 1:
        $this->stepStart('surname', $state);
        if ($text === '') {
          $data['text'] = 'Type your surname:';
          $result = Request::sendMessage($data);
          break;
        }
        $text = $this->stepStop($text);
        // No break!
      case 2:
        $this->stepStart('age', $state);
        if ($text === '' || !is_numeric($text)) {
          $data['text'] = 'Type your age:';
          if ($text !== '') {
            $data['text'] = 'Age must be a number';
          }
          $result = Request::sendMessage($data);
          break;
        }
        $text = $this->stepStop($text);
        // No break!
      case 3:
        $this->stepStart('gender', $state);
        $genders = ['M', 'F'];
        if ($text === '' || !in_array($text, $genders, TRUE)) {
          $data['reply_markup'] = (new Keyboard($genders))
            ->setResizeKeyboard(TRUE)
            ->setOneTimeKeyboard(TRUE)
            ->setSelective(TRUE);
          $data['text'] = 'Select your gender:';
          if ($text !== '') {
            $data['text'] = 'Choose a keyboard option to select your gender';
          }
          $result = Request::sendMessage($data);
          break;
        }
        $text = $this->stepStop($text);
        // No break!
      case 4:
        $this->stepStart('geo', $state);
        if ($message->getLocation() === NULL && $text != 'Skip') {
          $button = (new KeyboardButton('Share Location'))->setRequestLocation(TRUE);
          $skip = new KeyboardButton('Skip');
          $data['reply_markup'] = (new Keyboard($button, $skip))
            ->setOneTimeKeyboard(TRUE)
            ->setResizeKeyboard(TRUE)
            ->setSelective(TRUE);
          $data['text'] = 'Share your location:';
          $result = Request::sendMessage($data);
          break;
        }
        if ($message->getLocation()) {
          $latitude = $message->getLocation()->getLatitude();
          $longitude = $message->getLocation()->getLongitude();
          $text = $this->stepStop("{$latitude}|{$longitude}");
        }
        else {
          $text = $this->stepStop("skip");
        }
        // No break!
      case 5:
        $this->stepStart('photo', $state);
        if ($message->getPhoto() === NULL) {
          $data['text'] = 'Insert your picture:';
          $result = Request::sendMessage($data);
          break;
        }
        $photo = $message->getPhoto()[0];
        $photo_id = $photo->getFileId();
        $text = $this->stepStop($photo_id);
        // No break!
      case 6:
        $this->stepStart('phone', $state);
        if ($message->getContact() === NULL) {
          $button = (new KeyboardButton('Share Contact'))->setRequestContact(TRUE);
          $data['reply_markup'] = (new Keyboard($button))
            ->setOneTimeKeyboard(TRUE)
            ->setResizeKeyboard(TRUE)
            ->setSelective(TRUE);
          $data['text'] = 'Share your contact information:';
          $result = Request::sendMessage($data);
          break;
        }
        $phone = $message->getContact()->getPhoneNumber();
        $text = $this->stepStop($phone);
        // No break!
      case 7:
        $data['caption'] = "/Survey result:\n\n";
        $keys = ['name', 'surname', 'age', 'gender', 'geo', 'phone'];
        foreach ($keys as $key) {
          $val = $this->getVal($key);
          $k = ucfirst($key);
          $data['caption'] .= "$k: $val\n";
        }
        $data['photo'] = $this->getVal('photo');
        $result = Request::sendPhoto($data);
        // Stop, break!
        $this->setVal("state", "");
        $this->stopConversation();
        break;
    }

    return $result;
  }

  /**
   * Step start.
   */
  public function stepStart(string $name, int | string $state): string {
    $this->name = $name;
    $this->state = $state;
    // $this->setVal("state", $state);
    return $name;
  }

  /**
   * Step stop.
   */
  public function stepStop(string $text): string {
    $this->state++;
    $this->setVal($this->name, $text);
    $this->setVal("state", $this->state);
    return "";
  }

}
