<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;

/**
 * User "/inlinekeyboard" command.
 *
 * Display an inline keyboard with a few buttons.
 *
 * This command requires CallbackqueryCommand to work!
 *
 * @see CallbackqueryCommand.php
 */
class InlinekeyboardCommand extends UserCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'inlinekeyboard';
    $this->description = 'Show inline keyboard';
    $this->version = '0.2.0';
    $this->usage = '/inlinekeyboard';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    $inline_keyboard = new InlineKeyboard([
      [
        'text' => 'Inline Query (current chat)',
        'switch_inline_query_current_chat' => 'inline query...',
      ],
      [
        'text' => 'Inline Query (other chat)',
        'switch_inline_query' => 'inline query...',
      ],
    ], [
      [
        'text' => 'Callback',
        'callback_data' => 'identifier',
      ],
      [
        'text' => 'Open URL',
        'url' => 'https://github.com/php-telegram-bot/example-bot',
      ],
    ]);

    return $this->replyToChat('Inline Keyboard', [
      'reply_markup' => $inline_keyboard,
    ]);
  }

}
