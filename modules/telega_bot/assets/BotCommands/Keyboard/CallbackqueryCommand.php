<?php

namespace Longman\TelegramBot\Commands\SystemCommands;

use Drupal\telega\Utility\SystemCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;

/**
 * Callback query command.
 *
 * This command handles all callback queries sent via inline keyboard buttons.
 *
 * @see InlinekeyboardCommand.php
 */
class CallbackqueryCommand extends SystemCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'callbackquery';
    $this->description = 'Handle the callback query';
    $this->version = '1.2.0';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    // Callback query data can be fetched and handled accordingly.
    $callback_query = $this->getCallbackQuery();
    $callback_data  = $callback_query->getData();

    return $callback_query->answer([
      'text' => 'Content of the callback data: ' . $callback_data,
      // Randomly show (or not) as an alert.
      'show_alert' => (bool) random_int(0, 1),
      'cache_time' => 5,
    ]);
  }

}
