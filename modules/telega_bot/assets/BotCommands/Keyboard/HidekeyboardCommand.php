<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;

/**
 * User "/hidekeyboard" command.
 *
 * Command to hide the keyboard.
 */
class HidekeyboardCommand extends UserCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'hidekeyboard';
    $this->description = 'Hide the custom keyboard';
    $this->version = '0.2.0';
    $this->usage = '/hidekeyboard';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    // Remove the keyboard and send a message.
    return $this->replyToChat('Keyboard Hidden', [
      'reply_markup' => Keyboard::remove(),
    ]);
  }

}
