<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Commands\UserCommand;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;

/**
 * User "/forcereply" command.
 *
 * Force a reply to a message.
 */
class ForcereplyCommand extends UserCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'forcereply';
    $this->description = 'Force reply with reply markup';
    $this->version = '0.2.0';
    $this->usage = '/forcereply';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    // Force a reply to the sent message.
    return $this->replyToChat('Write something in reply:', [
      'reply_markup' => Keyboard::forceReply(),
    ]);
  }

}
