<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Drupal\telega\Utility\UserCommand;
use Longman\TelegramBot\Entities\Keyboard;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;

/**
 * User "/keyboard" command.
 *
 * Display a keyboard with a few buttons.
 */
class KeyboardCommand extends UserCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'keyboard';
    $this->description = 'Show a custom keyboard with reply markup';
    $this->version = '0.3.0';
    $this->usage = '/keyboard';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    /** @var \Longman\TelegramBot\Entities\Keyboard[] $keyboards */
    $keyboards = [];

    // Simple digits.
    $keyboards[] = new Keyboard(
      ['7', '8', '9'],
      ['4', '5', '6'],
      ['1', '2', '3'],
      [' ', '0', ' ']
    );

    // Digits with operations.
    $keyboards[] = new Keyboard(
      ['7', '8', '9', '+'],
      ['4', '5', '6', '-'],
      ['1', '2', '3', '*'],
      [' ', '0', ' ', '/']
    );

    // Short version with 1 button per row.
    $keyboards[] = new Keyboard('A', 'B', 'C');

    // Some different ways of creating rows and buttons.
    $keyboards[] = new Keyboard(
      ['text' => 'A'],
      'B',
      ['C', 'D']
    );

    // Buttons to perform Contact or Location sharing.
    $keyboards[] = new Keyboard([
      ['text' => 'Send my contact', 'request_contact' => TRUE],
      ['text' => 'Send my location', 'request_location' => TRUE],
    ]);

    // Shuffle our example keyboards and return a random one.
    shuffle($keyboards);
    $keyboard = end($keyboards)
      ->setResizeKeyboard(TRUE)
      ->setOneTimeKeyboard(TRUE)
      ->setSelective(FALSE);

    return $this->replyToChat('Press a Button!', [
      'reply_markup' => $keyboard,
    ]);
  }

}
