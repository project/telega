<?php

namespace Longman\TelegramBot\Commands\SystemCommands;

use Longman\TelegramBot\Entities\Payments\LabeledPrice;
use Longman\TelegramBot\Entities\Payments\ShippingOption;
use Drupal\telega\Utility\SystemCommand;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Entities\ServerResponse;

/**
 * Shipping query required for "/payment" command with flexible shipping method.
 *
 * In this command, you can perform any necessary verifications and checks
 * to adjust the available shipping options of the payment.
 *
 * If the user has a "Free Delivery" subscription or something like that.
 */
class ShippingqueryCommand extends SystemCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'shippingquery';
    $this->description = 'Shipping Query Handler';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    // Here you can check the shipping details
    // and adjust the Shipping Options accordingly.
    // For this demo, let's simply define some fixed shipping options,
    // a "Basic" and "Premium" shipping method.
    // If we do make certain checks, you can define the error message:.
    // return $this->getShippingQuery()->answer(false, [
    // 'error_message' => 'We do not ship to your location :-(',
    // ]);.
    return $this->getShippingQuery()->answer(TRUE, [
      'shipping_options' => [
        new ShippingOption([
          'id'     => 'basic',
          'title'  => 'Basic Shipping',
          'prices' => [
            new LabeledPrice(['label' => 'Basic Shipping', 'amount' => 800]),
          ],
        ]),
        new ShippingOption([
          'id'     => 'premium',
          'title'  => 'Premium Shipping',
          'prices' => [
            new LabeledPrice(['label' => 'Premium Shipping', 'amount' => 1500]),
            new LabeledPrice(['label' => 'Extra speedy', 'amount' => 300]),
          ],
        ]),
      ],
    ]);
  }

}
