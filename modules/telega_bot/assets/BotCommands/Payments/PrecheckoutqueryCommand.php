<?php

namespace Longman\TelegramBot\Commands\SystemCommands;

use Drupal\telega\Utility\SystemCommand;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Entities\ServerResponse;

/**
 * Pre-checkout query required for "/payment" command.
 *
 * In this command you can perform any necessary verifications and checks
 * to allow or disallow the final checkout and payment of the invoice.
 */
class PrecheckoutqueryCommand extends SystemCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'precheckoutquery';
    $this->description = 'Pre-Checkout Query Handler';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    // Simply approve, no need for any checks at this point.
    // If we do make certain checks, you can define the error message:
    // return $this->getPreCheckoutQuery()->answer(false, [
    // 'error_message' => 'Registration (or whatever) required...',
    // ]);.
    return $this->getPreCheckoutQuery()->answer(TRUE);
  }

}
