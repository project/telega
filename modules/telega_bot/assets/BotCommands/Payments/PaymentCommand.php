<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Drupal\telega\Utility\UserCommand;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Entities\Payments\LabeledPrice;
use Longman\TelegramBot\Entities\Payments\SuccessfulPayment;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;

/**
 * User "/payment" command.
 *
 * This command creates an invoice for the user using the Telegram Payments.
 *
 * You will have to set up a payment provider with @BotFather
 * Select your bot and then "Payments". Then choose the provider of your choice.
 *
 * @BotFather will then present you with a payment provider token.
 *
 * Copy this token and set it in your config.php file:
 * ['commands']['configs']['payment'] =>
 * ['payment_provider_token' => 'your_payment_provider_token_here']
 *
 * You will also need to copy the `Precheckoutquerycommand.php` file.
 */
class PaymentCommand extends UserCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'payment';
    $this->usage = '/payment';
    $this->description = 'Create an invoice for the user using Telegram Payments';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    // Who to send this invoice to. (Use the current user.)
    $chat_id = $this->getMessage()->getFrom()->getId();

    // The currency of this invoice.
    // Supported currencies:
    // https://core.telegram.org/bots/payments#supported-currencies
    $currency = 'EUR';

    // List all items that will be shown on your invoice.
    // Amounts are in cents. So 1 Euro would be put as 100.
    $prices = [
    // 1€
      new LabeledPrice(['label' => 'Small thing', 'amount' => 100]),
    // 20€
      new LabeledPrice(['label' => 'Bigger thing', 'amount' => 2000]),
    // 500€
      new LabeledPrice(['label' => 'Huge thing', 'amount' => 50000]),
    ];

    // Request a shipping address if necessary.
    $need_shipping_address = FALSE;

    // If pricing, depends on the shipping method chosen, set this to true.
    // You will also need to copy and adapt the `ShippingqueryCommand.php` file.
    $is_flexible = FALSE;

    // Send the actual invoice!
    // Adjust any parameters to your needs.
    return Request::sendInvoice([
      'chat_id'               => $chat_id,
      'title'                 => 'Payment with PHP Telegram Bot',
      'description'           => 'A simple invoice to test Telegram Payments',
      'payload'               => 'payment_demo',
      'start_parameter'       => 'payment_demo',
      'provider_token'        => $this->getConfig('payment_provider_token'),
      'currency'              => $currency,
      'prices'                => $prices,
      'need_shipping_address' => $need_shipping_address,
      'is_flexible'           => $is_flexible,
    ]);
  }

  /**
   * Send "Thank you" message to user who paid.
   *
   * Y'll need to add some code to `GenericmessageCommand::execute()`.
   * Check the `GenericmessageCommand.php` file included in this folder.
   *
   * @throws \Longman\TelegramBot\Exception\TelegramException
   */
  public static function handleSuccessfulPayment(SuccessfulPayment $payment, int $user_id): ServerResponse {
    // Send a message to the user after they have completed the payment.
    return Request::sendMessage([
      'chat_id' => $user_id,
      'text'    => 'Thank you for your order!',
    ]);
  }

}
