<?php

namespace Longman\TelegramBot\Commands\SystemCommands;

use Drupal\telega\Utility\SystemCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;

/**
 * New chat members command.
 *
 * Gets executed when a new member joins the chat.
 *
 * NOTE: This command must be called from GenericmessageCommand.php!
 * It is only in a separate command file for easier code maintenance.
 */
class NewchatmembersCommand extends SystemCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'newchatmembers';
    $this->description = 'New Chat Members';
    $this->version = '1.3.0';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    $message = $this->getMessage();
    $members = $message->getNewChatMembers();

    if ($message->botAddedInChat()) {
      return $this->replyToChat('Hi there, you BOT!');
    }

    $member_names = [];
    foreach ($members as $member) {
      $member_names[] = $member->tryMention();
    }

    return $this->replyToChat('Hi ' . implode(', ', $member_names) . '!');
  }

}
