<?php

namespace Longman\TelegramBot\Commands\SystemCommands;

use Drupal\telega\Utility\SystemCommand;
use Longman\TelegramBot\Entities\InlineQuery\InlineQueryResultArticle;
use Longman\TelegramBot\Entities\InlineQuery\InlineQueryResultContact;
use Longman\TelegramBot\Entities\InlineQuery\InlineQueryResultLocation;
use Longman\TelegramBot\Entities\InlineQuery\InlineQueryResultVenue;
use Longman\TelegramBot\Entities\InputMessageContent\InputTextMessageContent;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;

/**
 * Inline query command.
 *
 * Command that handles inline queries and returns a list of results.
 */
class InlinequeryCommand extends SystemCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'inlinequery';
    $this->description = 'Handle inline query';
    $this->version = '1.2.0';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    $inline_query = $this->getInlineQuery();
    $query        = $inline_query->getQuery();
    $results      = [];

    if ($query !== '') {
      // https://core.telegram.org/bots/api#inlinequeryresultarticle
      // Here you can put any other Input...MessageContent you like.
      // It will keep the style of an article,
      // but post the specific message type back to the user.
      $results[] = new InlineQueryResultArticle([
        'id'                    => '001',
        'title'                 => 'Simple text using InputTextMessageContent',
        'description'           => 'this will return Text',
        'input_message_content' => new InputTextMessageContent([
          'message_text' => 'The query that got you here: ' . $query,
        ]),
      ]);

      // https://core.telegram.org/bots/api#inlinequeryresultcontact
      $results[] = new InlineQueryResultContact([
        'id'           => '002',
        'phone_number' => '12345678',
        'first_name'   => 'Best',
        'last_name'    => 'Friend',
      ]);

      // https://core.telegram.org/bots/api#inlinequeryresultlocation
      $results[] = new InlineQueryResultLocation([
        'id'        => '003',
        'title'     => 'The center of the world!',
        'latitude'  => 40.866667,
        'longitude' => 34.566667,
      ]);

      // https://core.telegram.org/bots/api#inlinequeryresultvenue
      $results[] = new InlineQueryResultVenue([
        'id'        => '004',
        'title'     => 'No-Mans-Land',
        'address'   => 'In the middle of Nowhere',
        'latitude'  => 33,
        'longitude' => -33,
      ]);
    }

    return $inline_query->answer($results);
  }

}
