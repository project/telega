<?php

namespace Longman\TelegramBot\Commands\SystemCommands;

use Drupal\telega\Utility\SystemCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;

/**
 * Chosen inline result command.
 *
 * Gets executed when an item from an inline query is selected.
 */
class ChoseninlineresultCommand extends SystemCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'choseninlineresult';
    $this->description = 'Handle the chosen inline result';
    $this->version = '1.2.0';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    // Information about the chosen result is returned.
    $inline_query = $this->getChosenInlineResult();
    $query = $inline_query->getQuery();
    return parent::execute();
  }

}
