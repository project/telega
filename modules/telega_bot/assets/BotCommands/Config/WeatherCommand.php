<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Drupal\telega\Utility\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\TelegramLog;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;

/**
 * User "/weather" command.
 *
 * Get weather info for the location passed as the parameter.
 * A OpenWeatherMap.org API key is required for this command!
 */
class WeatherCommand extends UserCommand {

  /**
   * Base URI for OpenWeatherMap API.
   *
   * @var string
   */
  private $owmApiBaseUri = 'http://api.openweathermap.org/data/2.5/';

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'weather';
    $this->description = 'Show weather by location';
    $this->version = '1.3.0';
    $this->usage = '/weather <location>';
  }

  /**
   * Get weather data using HTTP request.
   */
  private function getWeatherData(string $location): string {
    $client = new Client(['base_uri' => $this->owmApiBaseUri]);
    $path   = 'weather';
    $query  = [
      'q'     => $location,
      'units' => 'metric',
      'APPID' => trim($this->getConfig('owm_api_key')),
    ];

    try {
      $response = $client->get($path, ['query' => $query]);
    }
    catch (RequestException $e) {
      TelegramLog::error($e->getMessage());

      return '';
    }

    return (string) $response->getBody();
  }

  /**
   * Get weather string from weather data.
   */
  private function getWeatherString(array $data): string {
    try {
      if (!(isset($data['cod']) && $data['cod'] === 200)) {
        return '';
      }
      // http://openweathermap.org/weather-conditions
      $conditions     = [
        'clear'        => ' ☀️',
        'clouds'       => ' ☁️',
        'rain'         => ' ☔',
        'drizzle'      => ' ☔',
        'thunderstorm' => ' ⚡️',
        'snow'         => ' ❄️',
      ];
      $conditions_now = strtolower($data['weather'][0]['main']);

      return sprintf(
        'The temperature in %s (%s) is %s°C' . PHP_EOL .
        'Current conditions are: %s%s',
        // City.
        $data['name'],
        // Country.
        $data['sys']['country'],
        // Temperature.
        $data['main']['temp'],
        // Description of weather.
        $data['weather'][0]['description'],
        $conditions[$conditions_now] ?? ''
      );
    }
    catch (\Exception $e) {
      TelegramLog::error($e->getMessage());

      return '';
    }
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    // Check to make sure the required OWM API key has been defined.
    $owm_api_key = $this->getConfig('owm_api_key');
    if (empty($owm_api_key)) {
      return $this->replyToChat('OpenWeatherMap API key not defined.');
    }

    $location = trim($this->getMessage()->getText(TRUE));
    if ($location === '') {
      return $this->replyToChat('You must specify a location as: ' . $this->getUsage());
    }

    $text = 'Cannot find weather for location: ' . $location;
    if ($weather_data = json_decode($this->getWeatherData($location), TRUE)) {
      $text = $this->getWeatherString($weather_data);
    }
    return $this->replyToChat($text);
  }

}
