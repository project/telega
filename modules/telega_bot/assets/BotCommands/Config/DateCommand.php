<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Drupal\telega\Utility\UserCommand;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\TelegramLog;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;

/**
 * User "/date" command.
 *
 * Shows the date and time of the location passed as the parameter.
 * A Google API key is required for this command!
 */
class DateCommand extends UserCommand {

  /**
   * Guzzle Client object.
   *
   * @var \GuzzleHttp\Client
   */
  private $client;

  /**
   * Base URI for Google Maps API.
   *
   * @var string
   */
  private $googleApiBaseUri = 'https://maps.googleapis.com/maps/api/';

  /**
   * The Google API Key from the command config.
   *
   * @var string
   */
  private $googleApiKey;

  /**
   * Date format.
   *
   * @var string
   */
  private $dateFormat = 'd-m-Y H:i:s';

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'date';
    $this->description = 'Show date/time by location';
    $this->version = '1.5.0';
    $this->usage = '/date <location>';
  }

  /**
   * Get coordinates of passed location.
   */
  private function getCoordinates(string $location): array {
    $path  = 'geocode/json';
    $query = ['address' => urlencode($location)];

    if ($this->googleApiKey !== NULL) {
      $query['key'] = $this->googleApiKey;
    }

    try {
      $response = $this->client->get($path, ['query' => $query]);
    }
    catch (RequestException $e) {
      TelegramLog::error($e->getMessage());

      return [];
    }

    if (!($data = $this->validateResponseData($response->getBody()))) {
      return [];
    }

    $result = $data['results'][0];
    $lat    = $result['geometry']['location']['lat'];
    $lng    = $result['geometry']['location']['lng'];
    $acc    = $result['geometry']['location_type'];
    $types  = $result['types'];

    return [$lat, $lng, $acc, $types];
  }

  /**
   * Get date for location passed via coordinates.
   *
   * @throws \Exception
   */
  private function getDate(string $lat, string $lng): array {
    $path = 'timezone/json';

    $date_utc  = new \DateTimeImmutable("", new \DateTimeZone('UTC'));
    $timestamp = $date_utc->format('U');

    $query = [
      'location'  => urlencode($lat) . ',' . urlencode($lng),
      'timestamp' => urlencode($timestamp),
    ];

    if ($this->googleApiBaseUri !== NULL) {
      $query['key'] = $this->googleApiKey;
    }

    try {
      $response = $this->client->get($path, ['query' => $query]);
    }
    catch (RequestException $e) {
      TelegramLog::error($e->getMessage());

      return [];
    }

    if (!($data = $this->validateResponseData($response->getBody()))) {
      return [];
    }

    $local_time = $timestamp + $data['rawOffset'] + $data['dstOffset'];

    return [$local_time, $data['timeZoneId']];
  }

  /**
   * Evaluate the response data and see if the request was successful.
   */
  private function validateResponseData(string $data): array {
    if (empty($data)) {
      return [];
    }

    $data = json_decode($data, TRUE);
    if (empty($data)) {
      return [];
    }

    if (isset($data['status']) && $data['status'] !== 'OK') {
      return [];
    }

    return $data;
  }

  /**
   * Get formatted date at the passed location.
   *
   * @throws \Exception
   */
  private function getFormattedDate(string $location): string {
    if ($location === NULL || $location === '') {
      return 'The time in nowhere is never';
    }

    [$lat, $lng] = $this->getCoordinates($location);
    if (empty($lat) || empty($lng)) {
      return 'It seems that in "' . $location . '" they do not have a concept of time.';
    }

    [$local_time, $timezone_id] = $this->getDate($lat, $lng);

    $date_utc = new \DateTimeImmutable(gmdate('Y-m-d H:i:s', $local_time), new \DateTimeZone($timezone_id));

    return 'The local time in ' . $timezone_id . ' is: ' . $date_utc->format($this->dateFormat);
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    // First we set up the necessary member variables.
    $this->client = new Client(['base_uri' => $this->googleApiBaseUri]);
    if (($this->googleApiKey = trim($this->getConfig('googleApiKey'))) === '') {
      $this->googleApiKey = NULL;
    }

    $message = $this->getMessage();

    $chat_id  = $message->getChat()->getId();
    $location = $message->getText(TRUE);

    $text = 'You must specify location in format: /date <city>';

    if ($location !== '') {
      $text = $this->getFormattedDate($location);
    }

    $data = [
      'chat_id' => $chat_id,
      'text'    => $text,
    ];

    return Request::sendMessage($data);
  }

}
