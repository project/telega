<?php

namespace Longman\TelegramBot\Commands\SystemCommands;

use Drupal\telega\Utility\SystemCommand;
use Longman\TelegramBot\Commands\UserCommands\PaymentCommand;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;

/**
 * Generic message command, in Conversation-related context.
 *
 * Gets executed when any type of message is sent.
 */
class GenericmessageCommand extends SystemCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'genericmessage';
    $this->description = 'Handle generic message';
    $this->version = '1.0.0';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    // Catch and handle any service messages here.
    $message = $this->getMessage();
    // The chat photo was deleted.
    $delete_chat_photo = $message->getDeleteChatPhoto();
    // The group has been created.
    $group_chat_created = $message->getGroupChatCreated();
    // The supergroup has been created.
    $supergroup_chat_created = $message->getSupergroupChatCreated();
    // The channel has been created.
    $channel_chat_created = $message->getChannelChatCreated();
    // Information about the payment.
    $successful_payment = $message->getSuccessfulPayment();

    // Conversation.
    $conversation = $this->getVal("conversation");
    if ($conversation) {
      return $this->telegram->executeCommand($conversation);
    }

    // Payment.
    if ($payment = $message->getSuccessfulPayment()) {
      $user_id = $message->getFrom()->getId();
      return PaymentCommand::handleSuccessfulPayment($payment, $user_id);
    }

    // Group:
    // Handle new chat members.
    if ($message->getNewChatMembers()) {
      return $this->getTelegram()->executeCommand('newchatmembers');
    }

    // Handle left chat members.
    if ($message->getLeftChatMember()) {
      return $this->getTelegram()->executeCommand('leftchatmember');
    }

    // The chat photo was changed.
    if ($new_chat_photo = $message->getNewChatPhoto()) {
      // Whatever...
    }

    // The chat title was changed.
    if ($new_chat_title = $message->getNewChatTitle()) {
      // Whatever...
    }

    // A message has been pinned.
    if ($pinned_message = $message->getPinnedMessage()) {
      // Whatever...
    }
    return Request::emptyResponse();
  }

}
