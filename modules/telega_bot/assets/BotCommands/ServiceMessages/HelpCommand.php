<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Entities\ServerResponse;
use Drupal\telega\Utility\UserCommand;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;

/**
 * User "/help" command.
 *
 * Command that lists all available commands in User and Admin sections.
 */
class HelpCommand extends UserCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'help';
    $this->description = 'Show bot commands help';
    $this->version = '1.4.0';
    $this->usage = '/help or /help <command>';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    $message     = $this->getMessage();
    $command_str = trim($message->getText(TRUE));

    // Admin commands shouldn't be shown in group chats.
    $safe_to_show = $message->getChat()->isPrivateChat();

    [$all_commands, $user_commands, $admin_commands] = $this->getUserAndAdminCommands();

    // If no command parameter is passed, show the list.
    if ($command_str === '') {
      $text = '*Commands List*:' . PHP_EOL;
      foreach ($user_commands as $user_command) {
        $text .= '/' . $user_command->getName() . ' - ' . $user_command->getDescription() . PHP_EOL;
      }

      if ($safe_to_show && count($admin_commands) > 0) {
        $text .= PHP_EOL . '*Admin Commands List*:' . PHP_EOL;
        foreach ($admin_commands as $admin_command) {
          $text .= '/' . $admin_command->getName() . ' - ' . $admin_command->getDescription() . PHP_EOL;
        }
      }

      $text .= PHP_EOL . 'For exact command help type: /help <command>';

      return $this->replyToChat($text, ['parse_mode' => 'markdown']);
    }

    $command_str = str_replace('/', '', $command_str);
    if (isset($all_commands[$command_str]) && ($safe_to_show || !$all_commands[$command_str]->isAdminCommand())) {
      $command = $all_commands[$command_str];

      return $this->replyToChat(sprintf(
            'Command: %s (v%s)' . PHP_EOL .
            'Description: %s' . PHP_EOL .
            'Usage: %s',
            $command->getName(),
            $command->getVersion(),
            $command->getDescription(),
            $command->getUsage()
        ), ['parse_mode' => 'markdown']);
    }

    return $this->replyToChat('No help available: Command `/' . $command_str . '` not found', ['parse_mode' => 'markdown']);
  }

  /**
   * Get all available User and Admin commands to display in the help list.
   *
   * @return \Longman\TelegramBot\Commands\Command[][]
   *   Commands array.
   *
   * @throws \Longman\TelegramBot\Exception\TelegramException
   */
  protected function getUserAndAdminCommands(): array {
    /** @var \Longman\TelegramBot\Commands\Command[] $all_commands */
    $all_commands = $this->telegram->getCommandsList();

    // Only get enabled Admin and User commands that are allowed to be shown.
    $commands = array_filter($all_commands, function ($command): bool {
        return !$command->isSystemCommand() && $command->showInHelp() && $command->isEnabled();
    });

    // Filter out all User commands.
    $user_commands = array_filter($commands, function ($command): bool {
        return $command->isUserCommand();
    });

    // Filter out all Admin commands.
    $admin_commands = array_filter($commands, function ($command): bool {
        return $command->isAdminCommand();
    });

    ksort($commands);
    ksort($user_commands);
    ksort($admin_commands);

    return [$commands, $user_commands, $admin_commands];
  }

}
