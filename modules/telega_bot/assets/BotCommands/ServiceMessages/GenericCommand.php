<?php

namespace Longman\TelegramBot\Commands\SystemCommands;

use Drupal\telega\Utility\SystemCommand;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Entities\ServerResponse;

/**
 * Generic command.
 *
 * Gets executed for generic commands, when no other appropriate one is found.
 */
class GenericCommand extends SystemCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'generic';
    $this->description = 'Handles generic commands or is executed by default when a command is not found';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    $message = $this->getMessage();
    $command = "";
    if (is_object($message)) {
      $user_id = $message->getFrom()->getId();
      $command = $message->getCommand();
    }

    // To enable proper use of the /whois command.
    // If admin and command like "/whoisXYZ", call the /whois command.
    if (stripos($command, 'whois') === 0 && $this->telegram->isAdmin($user_id)) {
      return $this->telegram->executeCommand('whois');
    }

    return $this->replyToChat("Command /{$command} not found.. :(");
  }

}
