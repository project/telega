## TODO:

- WeatherCommand
  - `$this->getConfig('owm_api_key')`
- DateCommand
  - `$this->getConfig('googleApiKey')`

# Telegram Payments

With the files in this folder, you can create and send invoices to your users, require their shipping details, define custom / flexible shipping methods and send a confirmation message after payment.

## Enable payments for your bot

Read all about Telegram Payments and follow the setup guide here:
https://core.telegram.org/bots/payments

## Configuring the `/payment` command

First of all, as a bare minimum, you need to copy the [`PaymentCommand.php`](PaymentCommand.php) and [`PrecheckoutqueryCommand.php`](PrecheckoutqueryCommand.php) files in this folder to your custom commands folder.

If you want to allow flexible shipping options, you will also need to copy [`ShippingqueryCommand.php`](ShippingqueryCommand.php) to your custom commands folder.

Should you want to send a message on a successful payment, you will need to copy the [`GenericmessageCommand.php`](GenericmessageCommand.php) file as well.
If you already have a `GenericmessageCommand.php` file, you'll need to copy the code from the `execute` method into your file.

Next, you will need to add the Payment Provider Token (that you received in the previous step when linking your bot), to your `hook.php` or `manager.php` config.

For `hook.php`:

```php
$telegram->setCommandConfig('payment', ['payment_provider_token' => 'your_payment_provider_token_here']);
```

For `manager.php` or using a general `config.php`, in the config array add:

```php
...
'commands' => [
    'configs' => [
        'payment' => ['payment_provider_token' => 'your_payment_provider_token_here'],
    ],
],
...
```

Now, when sending the `/payment` command to your bot, you should receive an invoice.

Have fun with Telegram Payments!

# Message

You bot can handle all types of messages.

## Private and Group chats

Messages in private and group chats get handled by [`GenericmessageCommand.php`](GenericmessageCommand.php).
When a message gets edited, it gets handled by [`EditedmessageCommand.php`](EditedmessageCommand.php)

(Have a look at [`EditmessageCommand.php`](EditmessageCommand.php) for an example of how to edit messages via your bot)

## Channels

For channels, the messages (or posts) get handled by [`ChannelpostCommand.php`](ChannelpostCommand.php).
When a channel post gets edited, it gets handled by [`EditedchannelpostCommand.php`](EditedchannelpostCommand.php)

# Keyboards

The files in this folder demonstrate how to create normal and inline keyboards.

## Normal Keyboard

Have a look at [`KeyboardCommand.php`](KeyboardCommand.php) for usage examples.

## Inline Keyboard

Have a look at [`InlinekeyboardCommand.php`](InlinekeyboardCommand.php) for usage examples.

To handle inline keyboard buttons, you need to handle all callbacks inside [`CallbackqueryCommand.php`](CallbackqueryCommand.php).

# Group or Channel

Requests specific to groups and channels all get handled in [`GenericmessageCommand.php`](GenericmessageCommand.php).

The two extra commands [`NewchatmembersCommand`](NewchatmembersCommand.php) and [`LeftchatmemberCommand`](LeftchatmemberCommand.php) are simply files that can be called as commands from within a command, not by a user.
Have a look at [`GenericmessageCommand.php`](GenericmessageCommand.php) to understand what you can do.

# Conversation

Conversations can be used to create dialogues with users, to collect information in a "conversational" style.

Look at the [`SurveyCommand`](SurveyCommand.php) to see how a conversation can be made.

For conversations to work, you must add the code provided in [`GenericmessageCommand.php`](GenericmessageCommand.php) at the beginning of your custom `GenericmessageCommand::execute()` method.

The [`CancelCommand`](CancelCommand.php) allows users to cancel any active conversation.

# Config

Custom configurations can be passed to commands that support them.

This feature is mainly used to pass secrets or special values to the commands.

## Adding configurations to your config

It is very easy to add configurations to `config.php`:

```php
'commands' => [
    'configs' => [
        'yourcommand' => ['your_config_key' => 'your_config_value'],
    ],
],
```

Alternatively, you can set them directly via code in your `hook.php`:

```php
$telegram->setCommandConfig('yourcommand', ['your_config_key' => 'your_config_value']);
```

## Reading configurations in your command

To read any command configurations, you can fetch them from within your command like this:

```php
$my_config = $this->getConfig('your_config_key'); // 'your_config_value'
```
