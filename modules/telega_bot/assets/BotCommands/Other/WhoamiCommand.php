<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\ChatAction;
use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;
use Drupal\telega\Utility\UserCommand;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;

/**
 * User "/whoami" command.
 *
 * Simple command that returns info about the current user.
 */
class WhoamiCommand extends UserCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'whoami';
    $this->description = 'Show your id, name and username';
    $this->version = '1.2.0';
    $this->usage = '/whoami';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    $message = $this->getMessage();

    $from       = $message->getFrom();
    $user_id    = $from->getId();
    $chat_id    = $message->getChat()->getId();
    $message_id = $message->getMessageId();

    $data = [
      'chat_id'             => $chat_id,
      'reply_to_message_id' => $message_id,
    ];

    // Send chat action "typing...".
    Request::sendChatAction([
      'chat_id' => $chat_id,
      'action'  => ChatAction::TYPING,
    ]);

    $caption = sprintf(
          'Your Id: %d' . PHP_EOL .
          'Name: %s %s' . PHP_EOL .
          'Username: %s',
          $user_id,
          $from->getFirstName(),
          $from->getLastName(),
          $from->getUsername()
    );

    // Fetch the most recent user profile photo.
    $limit  = 1;
    $offset = NULL;

    $user_profile_photos_response = Request::getUserProfilePhotos([
      'user_id' => $user_id,
      'limit'   => $limit,
      'offset'  => $offset,
    ]);

    if ($user_profile_photos_response->isOk()) {
      /** @var \Longman\TelegramBot\Entities\UserProfilePhotos $user_profile_photos */
      $user_profile_photos = $user_profile_photos_response->getResult();

      if ($user_profile_photos->getTotalCount() > 0) {
        $photos = $user_profile_photos->getPhotos();

        // Get the best quality of the profile photo.
        $photo   = end($photos[0]);
        $file_id = $photo->getFileId();

        $data['photo']   = $file_id;
        $data['caption'] = $caption;

        return Request::sendPhoto($data);
      }
    }

    // No Photo just send text.
    $data['text'] = $caption;

    return Request::sendMessage($data);
  }

}
