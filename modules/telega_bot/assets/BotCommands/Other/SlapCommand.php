<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Entities\ServerResponse;
use Drupal\telega\Utility\UserCommand;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;

/**
 * User "/slap" command.
 *
 * Slap a user around with a big trout!
 */
class SlapCommand extends UserCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'slap';
    $this->description = 'Slap someone with their username';
    $this->version = '1.2.0';
    $this->usage = '/slap <@user>';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    $message = $this->getMessage();
    $text    = $message->getText(TRUE);
    $sender  = '@' . $message->getFrom()->getUsername();

    // Username validation (simply checking for `@something` in the text)
    if (0 === preg_match('/@[\w_]{5,}/', $text)) {
      return $this->replyToChat('Sorry, no one to slap around...');
    }

    return $this->replyToChat("$sender slaps $text around a bit with a large trout");
  }

}
