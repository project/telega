<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Drupal\telega\Utility\UserCommand;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Entities\ServerResponse;

/**
 * User "/markdown" command.
 *
 * Print some text formatted with markdown.
 */
class MarkdownCommand extends UserCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'markdown';
    $this->description = 'Print Markdown Text';
    $this->version = '1.1.0';
    $this->usage = '/markdown';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    $text = '
*bold* _italic_ `inline fixed width code`

```
preformatted code block
code block
```

[Best Telegram bot api!!](https://github.com/php-telegram-bot/core)';
    return $this->replyToChat($text, [
      'parse_mode' => 'markdown',
    ]);
  }

}
