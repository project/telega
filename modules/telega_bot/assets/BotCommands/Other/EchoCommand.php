<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Drupal\telega\Utility\UserCommand;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Entities\ServerResponse;

/**
 * User "/echo" command.
 *
 * Simply echo the input back to the user.
 */
class EchoCommand extends UserCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'echo';
    $this->description = 'Show text';
    $this->version = '1.2.0';
    $this->usage = '/echo <text>';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    $message = $this->getMessage();
    $text    = $message->getText(TRUE);

    if ($text === '') {
      return $this->replyToChat('Command usage: ' . $this->getUsage());
    }

    return $this->replyToChat($text);
  }

}
