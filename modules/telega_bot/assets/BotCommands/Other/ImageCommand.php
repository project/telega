<?php

namespace Longman\TelegramBot\Commands\UserCommands;

use Longman\TelegramBot\Entities\ServerResponse;
use Longman\TelegramBot\Request;
use Drupal\telega\Utility\UserCommand;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;

/**
 * User "/image" command.
 *
 * Randomly fetch any uploaded image from the
 * Uploads path and send it to the user.
 */
class ImageCommand extends UserCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'image';
    $this->description = 'Randomly fetch any uploaded image';
    $this->version = '1.2.0';
    $this->usage = '/image';
    $this->private_only = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    $message = $this->getMessage();

    // Use any extra parameters as the caption text.
    $caption = trim($message->getText(TRUE));

    // Make sure the Upload path has been defined and exists.
    $upload_path = $this->telegram->getUploadPath();
    if (!is_dir($upload_path)) {
      return $this->replyToChat('Upload path has not been defined or does not exist.');
    }

    // Get a random picture from the Upload path.
    $random_image = $this->getRandomImagePath($upload_path);
    if ('' === $random_image) {
      return $this->replyToChat('No image found!');
    }

    // If no caption is set, use the filename.
    if ('' === $caption) {
      $caption = basename($random_image);
    }

    return Request::sendPhoto([
      'chat_id' => $message->getFrom()->getId(),
      'caption' => $caption,
      'photo'   => $random_image,
    ]);
  }

  /**
   * Return the path to a random image in the passed directory.
   */
  private function getRandomImagePath(string $dir): string {
    if (!is_dir($dir)) {
      return '';
    }

    // Filter the file list to only return images.
    $image_list = array_filter(scandir($dir), function ($file) {
        $extension = pathinfo($file, PATHINFO_EXTENSION);
        return in_array($extension, ['png', 'jpg', 'jpeg', 'gif']);
    });
    if (!empty($image_list)) {
      shuffle($image_list);
      return $dir . '/' . $image_list[0];
    }

    return '';
  }

}
