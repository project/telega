<?php

namespace Longman\TelegramBot\Commands\SystemCommands;

use Drupal\telega\Utility\SystemCommand;
use Longman\TelegramBot\Telegram;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Entities\ServerResponse;

/**
 * Start command, when a user first starts using the bot.
 */
class StartCommand extends SystemCommand {

  /**
   * {@inheritdoc}
   */
  public function __construct(Telegram $telegram, ?Update $update = NULL) {
    parent::__construct($telegram, $update);
    $this->name = 'start';
    $this->description = 'Start command';
    $this->usage = '/start';
  }

  /**
   * {@inheritdoc}
   */
  public function execute(): ServerResponse {
    // If you use deep-linking, get the parameter like this:
    // $deep_linking_parameter = $this->getMessage()->getText(true);
    // @see https://core.telegram.org/bots#deep-linking
    return $this->replyToChat(
          'Hi there!' . PHP_EOL .
          'Type /help to see all commands!'
      );
  }

}
