<?php

namespace Drupal\telega_bot\PluginManager;

use Drupal\Component\Plugin\PluginBase;

/**
 * Provides an TelegaBot plugin manager.
 *
 * @see Drupal\telega_bot\PluginManager\TelegaBotAnnotation
 * @see Drupal\telega_bot\PluginManager\TelegaBotPluginInterface
 * @see plugin_api
 */
class TelegaBotPluginBase extends PluginBase implements TelegaBotPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->commands = [];
  }

}
