<?php

namespace Drupal\telega_bot\PluginManager;

/**
 * Defines the common interface for all TelegaBot classes.
 *
 * @see \Drupal\telega_bot\PluginManager\TelegaBotManager
 * @see \Drupal\telega_bot\PluginManager\TelegaBotAnnotation
 * @see plugin_api
 */
interface TelegaBotPluginInterface {

}
