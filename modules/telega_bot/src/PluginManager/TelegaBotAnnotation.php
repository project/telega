<?php

namespace Drupal\telega_bot\PluginManager;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines an telega_bot Commands.
 *
 * Plugin Namespace: Plugin\TelegaBot.
 *
 * For a working example, see \Drupal\system\Plugin\TelegaBot\Demo
 *
 * @see \Drupal\telega_bot\PluginManager\TelegaBotManager
 * @see \Drupal\telega_bot\PluginManager\TelegaBotPluginInterface
 * @see plugin_api
 * @see hook_archiver_info_alter()
 *
 * @Annotation
 */
class TelegaBotAnnotation extends Plugin {

  /**
   * The archiver plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The human-readable name of the archiver plugin.
   *
   * @ingroup plugin_translatable
   *
   * @var \Drupal\Core\Annotation\Translation
   */
  public $title;

}
