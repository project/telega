<?php

namespace Drupal\telega_bot\PluginManager;

use Drupal\Component\Plugin\Factory\DefaultFactory;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Provides an Bot plugin manager.
 *
 * @see \Drupal\telega_bot\PluginManager\TelegaBotPluginInterface
 * @see \Drupal\telega_bot\PluginManager\TelegaBotManager
 * @see plugin_api
 */
class TelegaBotManager extends DefaultPluginManager {

  /**
   * Constructs a ArchiverManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cache_backend,
    ModuleHandlerInterface $module_handler
  ) {
    parent::__construct(
      'Plugin/TelegaBot',
      $namespaces,
      $module_handler,
      'Drupal\telega_bot\PluginManager\TelegaBotPluginInterface',
      'Drupal\telega_bot\PluginManager\TelegaBotAnnotation'
    );
    $this->alterInfo('telega_bot_info');
    $this->setCacheBackend($cache_backend, 'telega_bot_plugin');
    $this->factory = new DefaultFactory($this->getDiscovery());
  }

  /**
   * Bots - Plugable Options.
   */
  public function optionsList() {
    $options = [];
    foreach ($this->getDefinitions() as $key => $plugin) {
      $options[$key] = $plugin['title']->__toString();
    }
    return $options;
  }

}
