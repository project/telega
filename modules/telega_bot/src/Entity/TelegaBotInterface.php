<?php

namespace Drupal\telega_bot\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a bot entity type.
 */
interface TelegaBotInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the bot title.
   *
   * @return string
   *   Title of the bot.
   */
  public function getTitle();

  /**
   * Sets the bot title.
   *
   * @param string $title
   *   The bot title.
   *
   * @return \Drupal\telega_bot\Entity\TelegaBotInterface
   *   The called bot entity.
   */
  public function setTitle($title);

  /**
   * Gets the bot creation timestamp.
   *
   * @return int
   *   Creation timestamp of the bot.
   */
  public function getCreatedTime();

  /**
   * Sets the bot creation timestamp.
   *
   * @param int $timestamp
   *   The bot creation timestamp.
   *
   * @return \Drupal\telega_bot\Entity\TelegaBotInterface
   *   The called bot entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the bot status.
   *
   * @return bool
   *   TRUE if the bot is enabled, FALSE otherwise.
   */
  public function isEnabled();

  /**
   * Sets the bot status.
   *
   * @param bool $status
   *   TRUE to enable this bot, FALSE to disable.
   *
   * @return \Drupal\telega_bot\Entity\TelegaBotInterface
   *   The called bot entity.
   */
  public function setStatus($status);

  /**
   * Search params in yaml.
   *
   * @param string $param
   *   Param string, example `chatwoot/inbox`.
   */
  public function getYamlParam(string $param) : NULL |  string | array;

}
