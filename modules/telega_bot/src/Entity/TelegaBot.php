<?php

namespace Drupal\telega_bot\Entity;

use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\telega\Utility\ContentEntity;
use Drupal\telega\Utility\FieldDefinition;

/**
 * Defines the bot entity class.
 *
 * @ContentEntityType(
 *   id = "telega_bot",
 *   label = @Translation("Bot"),
 *   label_collection = @Translation("Bots"),
 *   handlers = {
 *     "view_builder" = "Drupal\telega_bot\Entity\TelegaBotViewBuilder",
 *     "list_builder" = "Drupal\telega_bot\Entity\TelegaBotListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\telega_bot\Form\TelegaBotForm",
 *       "edit" = "Drupal\telega_bot\Form\TelegaBotForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "telega_bot",
 *   admin_permission = "administer bot",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   links = {
 *     "add-form" = "/telega/bot/add",
 *     "canonical" = "/telega/bot/{telega_bot}",
 *     "edit-form" = "/telega/bot/{telega_bot}/edit",
 *     "delete-form" = "/telega/bot/{telega_bot}/delete",
 *     "collection" = "/telega/bot"
 *   },
 *   field_ui_base_route = "entity.telega_bot.settings"
 * )
 */
class TelegaBot extends ContentEntity implements TelegaBotInterface {

  /**
   * {@inheritdoc}
   */
  public function getYamlParam(string $param) : NULL |  string | array {
    $yaml = $this->get('yaml')->value;
    $data = Yaml::decode($yaml ?? '');
    $keys = explode("/", $param);
    foreach ($keys as $key) {
      if (!empty($data[$key])) {
        $data = $data[$key];
      }
      else {
        $data = NULL;
      }
    }
    return $data;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {
    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = FieldDefinition::string('Title');
    $fields['status'] = FieldDefinition::status();
    $fields['uid'] = FieldDefinition::uid();
    $fields['created'] = FieldDefinition::created();
    $fields['changed'] = FieldDefinition::changed();
    // Custom.
    $types = \Drupal::service('plugin.manager.telega_bot')->optionsList();
    $fields['name'] = FieldDefinition::string('Name');
    $fields['token'] = FieldDefinition::string('Token');
    $fields['type'] = FieldDefinition::list('Type', $types);
    $fields['save_chats'] = FieldDefinition::list('Type', $types);
    $fields['save_messaegs'] = FieldDefinition::list('Type', $types);
    $fields['yaml'] = FieldDefinition::long('Yaml');
    $desctription = "If you want to add custom commands:
<pre>commands:
  my_module: DirectoryName
chatwoot:
  inbox: INBOX-ID
</pre>
Bot will try to find commands in `my_module/assets/DirectoryName/*`";
    $fields['yaml']->setDescription($desctription);
    return $fields;
  }

}
