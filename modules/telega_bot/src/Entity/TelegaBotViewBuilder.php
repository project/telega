<?php

namespace Drupal\telega_bot\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityViewBuilder;

/**
 * Provides a view controller for TelegaBot entity type.
 */
class TelegaBotViewBuilder extends EntityViewBuilder {

  /**
   * {@inheritdoc}
   */
  protected function getBuildDefaults(EntityInterface $entity, $view_mode) {
    $build = parent::getBuildDefaults($entity, $view_mode);
    if ($view_mode == 'full') {
      $bot = \Drupal::service('telega_bot.webhook')->getBot($entity);
      $f = 'Drupal\telega_bot\Form';
      $from = \Drupal::formBuilder()->getForm("$f\BotForm", $entity);
      $build['form'] = $from;
      $commands = [];
      foreach ($bot->getCommandsList() as $key => $command) {
        $name = explode("\\", get_class($command));
        $reflection = new \ReflectionClass(get_class($command));
        $file = str_replace(DRUPAL_ROOT, "", $reflection->getFileName());
        $commands[$key] = [
          'usage' => $command->getUsage(),
          'description' => $command->getDescription(),
          'private' => $command->isPrivateOnly() ? "TRUE" : "",
          'system' => $command->isSystemCommand() ? "TRUE" : "",
          'class' => [
            'data' => [
              '#markup' => end($name) . "<br><small>$file</small>",
            ],
          ],
        ];
      }
      usort($commands, function ($a, $b) {
        return $a['system'] <=> $b['system'];
      });

      $build['commands_list'] = [
        '#prefix' => '<h2>Bot Commands</h2>',
        '#theme' => 'table',
        '#header' => [
          'usage' => 'Usage',
          'description' => 'Description',
          'private' => 'Private',
          'system' => 'System',
          'class' => 'Class',
        ],
        '#rows' => $commands,
      ];
      $command_path = [];
      foreach ($bot->getCommandsPaths() as $path) {
        $command_path[] = str_replace(DRUPAL_ROOT, "", $path);
      }
      $command_path[] = [
        '#markup' => "<i>You can add custom commands directory in configuration</i>",
      ];
      $build['commands_path'] = [
        '#title' => "Command directories:",
        '#theme' => 'item_list',
        '#items' => $command_path,
      ];
    }
    return $build;
  }

}
