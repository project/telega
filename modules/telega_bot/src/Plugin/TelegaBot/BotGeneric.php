<?php

namespace Drupal\telega_bot\Plugin\TelegaBot;

use Drupal\telega_bot\PluginManager\TelegaBotPluginBase;
use Drupal\telega_bot\PluginManager\TelegaBotPluginInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Provides a 'Generic' Bot.
 *
 * @TelegaBotAnnotation(
 *   id = "generic",
 *   title = @Translation("Generic"),
 * )
 */
class BotGeneric extends TelegaBotPluginBase implements TelegaBotPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $module_handler = \Drupal::service('module_handler');
    $bot = $configuration['bot'];
    if ($yaml = $bot->yaml->value) {
      $settings = Yaml::parse($yaml);
      if (!empty($settings['commands'])) {
        foreach ($settings['commands'] as $module_name => $dir) {
          if ($module_handler->moduleExists($module_name)) {
            $path = $module_handler->getModule($module_name)->getPath();
            $fillpath = DRUPAL_ROOT . "/$path/assets/$dir";
            if (is_dir($fillpath)) {
              $this->commands[] = $fillpath;
            }
          }
        }
      }
    }
    if (empty($this->commands)) {
      $path = $module_handler->getModule('telega_bot')->getPath();
      $fillpath = DRUPAL_ROOT . "/$path/assets/BotGeneric";
      if (is_dir($fillpath)) {
        $this->commands[] = $fillpath;
      }
    }
    return $this;
  }

}
