<?php

namespace Drupal\telega_bot\Plugin\TelegaBot;

use Drupal\telega_bot\PluginManager\TelegaBotPluginBase;
use Drupal\telega_bot\PluginManager\TelegaBotPluginInterface;

/**
 * Provides a 'Demo' Bot.
 *
 * @TelegaBotAnnotation(
 *   id = "demo",
 *   title = @Translation("Demo"),
 * )
 */
class BotDemo extends TelegaBotPluginBase implements TelegaBotPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition) {
    $path = \Drupal::service('module_handler')
      ->getModule('telega_bot')
      ->getPath();
    $this->commands = [
      DRUPAL_ROOT . "/$path/assets/BotCommands",
    ];
  }

}
