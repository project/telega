<?php

namespace Drupal\telega_bot\Controller;

/**
 * @file
 * Contains \Drupal\telega_bot\Controller\BotWebhook.
 */

use Drupal\Component\Serialization\Json;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Controller AppPage.
 */
class BotWebhook extends ControllerBase {

  /**
   * Render Page.
   */
  public function page(Request $request) {
    return [
      'text' => ['#markup' => "<pre>++</pre>"],
    ];
  }

  /**
   * WebHook.
   */
  public function webhook($bot, Request $request) {
    $content = $request->getContent();
    $data = Json::decode($content);
    $message = [
      'name' => $bot->name->value,
      'chatwoot/inbox' => $bot->getYamlParam('chatwoot/inbox'),
    ];
    if (!empty($data)) {
      try {
        \Drupal::service('telega_bot')->saveData($bot, $data);
      }
      catch (\Throwable $th) {
        // Throw $th;.
      }
      $message = \Drupal::service('telega_bot.webhook')->handle($bot, $data);
    }
    return new JsonResponse($message);
  }

}
