<?php

namespace Drupal\telega_bot\Service;

use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Longman\TelegramBot\Entities\Update;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Telegram;

/**
 * Bot service.
 */
class BotWebhook {

  /**
   * Telegram.
   *
   * @var \Longman\TelegramBot\Telegram
   */
  protected $telegram;
  //phpcs:disable
  protected int $id;
  protected string $df;
  protected string $key;
  protected string $name;
  protected EntityInterface $entity;
  protected EntityTypeManagerInterface $etype;
  protected DateFormatter $d;
  //phpcs:enable

  /**
   * Creates a new Bot manager.
   */
  public function __construct() {
    $this->etype = \Drupal::service('entity_type.manager');
    $this->d = \Drupal::service('date.formatter');
    $this->df = DateTimeItemInterface::DATETIME_STORAGE_FORMAT;
  }

  /**
   * Init a new Bot manager.
   */
  public function init(EntityInterface $entity) {
    $this->entity = $entity;
    $this->id = $entity->id();
    $this->key = $entity->token->value;
    $this->name = $entity->name->value;
    try {
      $telegram = new Telegram($this->key, $this->name);
      // Set custom Upload and Download paths.
      $telegram->setDownloadPath(DRUPAL_ROOT . '/sites/default/private/telega/download');
      $telegram->setUploadPath(DRUPAL_ROOT . '/sites/default/private/telega/upload');
      $type = $entity->type->value;
      if ($type) {
        $plugin_manager = \Drupal::service('plugin.manager.telega_bot');
        $plugin = $plugin_manager->createInstance($type, ['bot' => $entity]);
        $commands_paths = $plugin->commands;
        $telegram->addCommandsPaths($commands_paths);
        $telegram->enableLimiter();
        $telegram->bot = $entity;
      }
      $this->telegram = $telegram;
    }
    catch (TelegramException $e) {
      $error = $e->getMessage();
      \Drupal::logger('telega')->notice("init fail $error " . $this->id);
      return $error;
    }
    return $this;
  }

  /**
   * Set webhook.
   */
  public function setup() {
    try {
      $id = $this->id;
      $host = \Drupal::request()->getSchemeAndHttpHost();
      $webhook = "$host/telega/bot/$id/webhook";
      $result = $this->telegram->setWebhook($webhook);
      if ($result->isOk()) {
        return $result->getDescription();
      }
      else {
        return "error";
      }
    }
    catch (TelegramException $e) {
      return $e->getMessage();
    }
  }

  /**
   * Webhook handle.
   */
  public function getBot(EntityInterface $bot) {
    $this->init($bot);
    return $this->telegram;
  }

  /**
   * Webhook handle.
   */
  public function handle(EntityInterface $bot, array $data) {
    $this->init($bot);
    \Drupal::logger('telega')->notice(json_encode($data));
    try {
      $response = $this->telegram->processUpdate(new Update($data, $this->name));
      return $response->toJson();
    }
    catch (TelegramException $e) {
      $message = $e->getMessage();
      $message = \Drupal::logger('telega_bot')->error($message);
      return $message;
    }
  }

}
