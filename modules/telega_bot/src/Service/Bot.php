<?php

namespace Drupal\telega_bot\Service;

use Drupal\Component\Serialization\Json;
use Drupal\Component\Serialization\Yaml;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\file\FileInterface;
use Drupal\telega_bot\Entity\TelegaBot;
use Longman\TelegramBot\Entities\InlineKeyboard;
use Longman\TelegramBot\Exception\TelegramException;
use Longman\TelegramBot\Request;
use Longman\TelegramBot\Telegram;

/**
 * Bot saver service.
 */
class Bot {

  /**
   * Telegram.
   *
   * @var \Longman\TelegramBot\Telegram
   */
  protected $telegram;


  //phpcs:disable
  protected int $id;
  protected string $df;
  protected string $key;
  protected string $name;
  protected bool $skipChatwoot = FALSE;
  protected TelegaBot $entity;
  protected EntityTypeManagerInterface $etype;
  protected DateFormatter $d;
  //phpcs:enable

  /**
   * Creates a new Bot manager.
   */
  public function __construct() {
    $this->etype = \Drupal::entityTypeManager();
    $this->d = \Drupal::service('date.formatter');
    $this->df = DateTimeItemInterface::DATETIME_STORAGE_FORMAT;

    // Enable admin users
    // $telegram->enableAdmins($admin_users);
    // Here you can set some command specific parameters
    // e.g. Google geocode/timezone api key for /date command
    // $telegram->setCommandConfig('date', [
    // 'google_api_key' => 'your_google_api_key_here'
    // ]);
    // End Extra, see
    // https://github.com/php-telegram-bot/example-bot
    // https://github.com/php-telegram-bot/core //.
  }

  /**
   * Init a new Bot manager.
   */
  public function init(EntityInterface $entity) {
    $this->entity = $entity;
    $this->id = $entity->id();
    $this->key = $entity->token->value;
    $this->name = $entity->name->value;
    /** @var \Longman\TelegramBot\Telegram $telegram */
    try {
      $telegram = new Telegram($this->key, $this->name);
      $this->telegram = $telegram;
    }
    catch (TelegramException $e) {
      $error = $e->getMessage();
      \Drupal::logger('telega')->notice("init fail $error " . $this->id);
      return $error;
    }
    return $this;
  }

  /**
   * Check Valuer in array.
   */
  private function getVal($data, $key) {
    $result = NULL;
    if (!empty($data[$key])) {
      $result = $data[$key];
    }
    return $result;
  }

  /**
   * Check Valuer in array.
   */
  private function getTitle($data, $type) {
    $result = FALSE;
    $bot = $this->name;
    $title = "";
    $n = [];
    if (!empty($data['first_name'])) {
      $n[] = $data['first_name'];
    }
    if (!empty($data['last_name'])) {
      $n[] = $data['last_name'];
    }
    if (!empty($data['username'])) {
      $n[] = "@{$data['username']}";
    }
    if (empty($n)) {
      $n[] = $data['id'];
    }
    $name = implode(" ", $n);
    if ($type == 'user') {
      $title = $name;
    }
    if ($type == 'chat') {
      if (!empty($data['title'])) {
        $title = $data['title'];
      }
      else {
        $title = "{$name} | @$bot";
      }
    }
    return $title;
  }

  /**
   * Create telega_user from user data array.
   */
  private function createUser(array $data) {
    if (empty($data['id'])) {
      return NULL;
    }
    $storage = $this->etype->getStorage('telega_user');
    $user = $this->query($storage, $data['id']);
    if ($user) {
      return $user;
    }
    $user = $storage->create([
      'title' => $this->getTitle($data, 'user'),
      'key' => $data['id'],
      'bot' => $data['is_bot'] ?? FALSE,
      'name' => $this->getVal($data, 'username'),
      'first_name' => $this->getVal($data, 'first_name'),
      'last_name' => $this->getVal($data, 'last_name'),
      'lang' => $this->getVal($data, 'language_code'),
    ]);
    $user->save();
    return $user;
  }

  /**
   * User.
   */
  private function user($message) {
    if (!empty($message['from'])) {
      $storage = $this->etype->getStorage('telega_user');
      $u = $message['from'];
      $user = $this->query($storage, $u['id']);
      if (!$user) {
        $user = $storage->create([
          'title' => $this->getTitle($u, 'user'),
          'key' => $u['id'],
          'bot' => $u['is_bot'],
          'name' => $this->getVal($u, 'username'),
          'first_name' => $this->getVal($u, 'first_name'),
          'last_name' => $this->getVal($u, 'last_name'),
          'lang' => $this->getVal($u, 'language_code'),
          // 'yaml' => Yaml::dump($u, 8),
          // 'data' => FALSE,
        ]);
        $user->save();
      }
      return $user;
    }
  }

  /**
   * Chat.
   */
  private function chat($message) {
    if (!empty($message['chat'])) {
      $c = $message['chat'];
      $storage = $this->etype->getStorage('telega_chat');
      $chat = $this->query($storage, $c['id'], $c['type'] == 'private');
      if (!$chat) {
        $chat = $storage->create([
          'title' => $this->getTitle($c, 'chat'),
          'key' => $c['id'],
          'type' => $c['type'],
          'name' => $this->getVal($c, 'username'),
          'first_name' => $this->getVal($c, 'first_name'),
          'last_name' => $this->getVal($c, 'last_name'),
          'bot_id' => $this->id,
          'all_admins' => FALSE,
          'key_old' => FALSE,
          // 'yaml' => Yaml::dump($c, 8),
          // 'data' => $c,
        ]);
        $chat->save(TRUE);
      }
      return $chat;
    }
  }

  /**
   * Chat.
   */
  private function getReplyToMessage(array $message) :? int {
    if (empty($message['reply_to_message']['message_id'])) {
      return NULL;
    }
    $storage = $this->etype->getStorage('telega_message');
    $reply_to_message = $this->query(
      $storage, $message['reply_to_message']['message_id']
    );
    if (!$reply_to_message) {
      $m = $message['reply_to_message'];
      $user = $this->user($m);
      $chat = $this->chat($m);
      $title = mb_substr($this->getVal($m, 'text'), 0, 40);
      $reply_to_message = $storage->create([
        'title' => !empty($title) ? $title : '—',
        'key' => $m['message_id'],
        'date' => $this->d->format($m['date'], 'custom', $this->df),
        'text' => $this->getVal($m, 'text') ?? $title,
        'bot_id' => $this->id,
        'chat_id' => $chat->id(),
        'user_id' => $user->id(),
        'entities' => $this->getVal($m, 'entities'),
      ]);
      $reply_to_message->save(TRUE);
    }
    return $reply_to_message->id();
  }

  /**
   * Query.
   */
  private function query($storage, $key, $bot = FALSE) {
    $entities = [];
    $query = $storage->getQuery()
      ->condition('status', 1)
      ->condition('key', $key)
      ->accessCheck(TRUE)
      ->range(0, 1);
    if ($bot) {
      $query->condition('bot_id', $this->id);
    }
    $ids = $query->execute();
    if (!empty($ids)) {
      foreach ($storage->loadMultiple($ids) as $id => $entity) {
        $entities[$id] = $entity;
      }
    }
    return array_shift($entities);
  }

  /**
   * Send message.
   */
  public function send(
    string $chat,
    string $text,
    array $options = [],
  ) : array {
    $parameters = array_merge([
      'chat_id' => $chat,
      'text'    => $text,
    ], $options);
    $data = $this->sendMessage($parameters);
    return $data;
  }

  /**
   * Send Image from URL.
   */
  public function sendImage(string $chat, string $text, string $url) : array {
    $json = Request::sendPhoto([
      'chat_id' => $chat,
      'photo'   => $url,
      'caption' => $text,
    ]);
    $data = Json::decode($json);
    if (!$data['ok']) {
      \Drupal::logger(__CLASS__ . __FUNCTION__)->notice(
      '[Chat:@chat] Error: @er | URL: @url', [
        '@chat' => $chat,
        '@er' => "{$data['error_code']} : {$data['description']}",
        '@url' => $url,
      ]);
      $data = $this->sendMessage([
        'chat_id' => $chat,
        'text'    => $url,
      ]);
    }
    return $data;
  }

  /**
   * Send Image from URL.
   */
  public function sendAudio(string $chat, string $text, string $url) : array {
    // @todo Wav to mp3.
    $json = Request::sendAudio([
      'chat_id' => $chat,
      'audio'   => $url,
      'caption' => $text,
    ]);
    $data = Json::decode($json);
    if (!$data['ok']) {
      \Drupal::logger(__CLASS__ . __FUNCTION__)->notice(
      '[Chat:@chat] Error: @er | URL: @url', [
        '@chat' => $chat,
        '@er' => "{$data['error_code']} : {$data['description']}",
        '@url' => $url,
      ]);
      $data = $this->sendMessage([
        'chat_id' => $chat,
        'text'    => $url,
      ]);
    }
    return $data;
  }

  /**
   * Send Image from URL.
   */
  public function sendFile(string $chat, string $text, string $url) : array {
    $json = Request::sendDocument([
      'chat_id' => $chat,
      'document' => $url,
      'caption' => $text,
    ]);
    $data = Json::decode($json);
    if (!$data['ok']) {
      \Drupal::logger(__CLASS__ . __FUNCTION__)->notice(
      '[Chat:@chat] Error: @er | URL: @url', [
        '@chat' => $chat,
        '@er' => "{$data['error_code']} : {$data['description']}",
        '@url' => $url,
      ]);
      $data = $this->sendMessage([
        'chat_id' => $chat,
        'text'    => $url,
      ]);
    }
    return $data;
  }

  /**
   * Send message.
   */
  public function sendInlineKeyboard(string $chat, string $text) : array {
    $kboard = [
      ['text' => '🔴', 'callback_data' => 'b1'],
      ['text' => '🔵 2', 'callback_data' => 'b2'],
      ['text' => '3 🟢', 'callback_data' => 'b3'],
    ];
    $kboard2 = [
      ['text' => '4', 'callback_data' => 'b4'],
      ['text' => '5 ⭕️', 'callback_data' => 'b5'],
      ['text' => '6', 'callback_data' => 'b6'],
    ];
    $kboard3 = [
      ['text' => '7', 'callback_data' => 'b7'],
      ['text' => '8', 'callback_data' => 'b8'],
      ['text' => '9', 'callback_data' => 'b9'],
    ];
    $inline_keyboard = new InlineKeyboard($kboard, $kboard2, $kboard3);
    $data = $this->sendMessage([
      'chat_id' => $chat,
      'text'    => $text,
      'reply_markup' => $inline_keyboard,
    ]);
    return $data;
  }

  /**
   * Get available attachment type.
   */
  private function getAttachmentType(array $message) :? string {
    $available_types = ['audio', 'document', 'photo', 'video', 'voice'];
    $message_keys = array_keys($message);
    $types = array_intersect($available_types, $message_keys);
    if (empty($types)) {
      return NULL;
    }
    return array_shift($types);
  }

  /**
   * Get attachment file_id.
   */
  private function getAttachmentFileId(array $message) :? string {
    $attachment_type = $this->getAttachmentType($message);
    if (empty($attachment_type)) {
      return NULL;
    }
    $attachment = $message[$attachment_type];
    if ($attachment_type == 'photo') {
      $attachment = end($attachment);
    }
    return $attachment['file_id'];
  }

  /**
   * Get photo file.
   */
  private function getAttachment(array $message) :? FileInterface {
    $config = \Drupal::config('telega.settings');
    if (!$config->get('fs-enable')) {
      return NULL;
    }
    $file_id = $this->getAttachmentFileId($message);
    if (empty($file_id)) {
      return NULL;
    }
    $download_path = implode('/', [
      \Drupal::service('file_system')->getTempDirectory(),
      hash('sha256', $file_id, FALSE),
    ]);
    $this->telegram->setDownloadPath('telega_message/attachment');
    $download_path = $this->telegram->getDownloadPath();
    $file = Request::getFile(['file_id' => $file_id]);
    if (!$file->isOk()) {
      return NULL;
    }
    elseif (!Request::downloadFile($file->getResult())) {
      return NULL;
    }
    $filepath = $download_path . '/' . $file->getResult()->getFilePath();
    $scheme = $config->get('fs-scheme') ?: 'public';
    $date = \Drupal::service('date.formatter')->format(time(), 'custom', 'Y/m/d');
    $directory = "$scheme://telega_message/attachment/$date/";
    \Drupal::service('file_system')
      ->prepareDirectory(
        $directory, FileSystemInterface:: CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS
      );
    $data = file_get_contents($filepath);
    $filename = \Drupal::service('file_system')
      ->basename($filepath);
    $file = \Drupal::service('file.repository')
      ->writeData($data, $directory . $filename, FileExists::Rename);
    unlink($filepath);
    return $file;
  }

  /**
   * Get message text.
   */
  private function getMessageText(array $message) :? string {
    return implode(PHP_EOL, [
      $this->getVal($message, 'caption'),
      $this->getVal($message, 'text'),
    ]);
  }

  /**
   * Save Data.
   */
  public function saveData(EntityInterface $bot, array $data) {
    $this->init($bot);
    if (!empty($data['message'])) {
      $m = $data['message'];
      $user = $this->user($m);
      $chat = $this->chat($m);
      $title = mb_substr($this->getVal($m, 'text'), 0, 40);
      $yaml = "";
      if (!empty($m['contact'])) {
        $yaml = Yaml::encode($m['contact'], 8);
        $title = implode(" / ", $m['contact']);
      }
      $attach = \Drupal::service('telega')->parseMessageAttach($m);
      if (!empty($attach)) {
        $yaml = Yaml::encode($attach, 8);
        if (!$title) {
          $title = $attach['title'] ?? 'attach';
        }
      }
      $message = [
        'title' => !empty($title) ? $title : '—',
        'key' => $m['message_id'],
        'date' => $this->d->format($m['date'], 'custom', $this->df),
        'text' => $this->getMessageText($m),
        'bot_id' => $this->id,
        'chat_id' => $chat->id(),
        'user_id' => $user->id(),
        'reply_to_message_id' => $this->getReplyToMessage($m),
        'attachment' => $this->getAttachment($m),
        'entities' => $this->getVal($m, 'entities'),
        'yaml' => $yaml,
      ];
      $this->saveMessageEntity($message);
    }
    elseif (!empty($data['callback_query']['message'])) {
      $m = $data['callback_query']['message'];
      $user = $this->user($m);
      $chat = $this->chat($m);
      $data = $data['callback_query']['data'];
      $message = [
        'title' => 'callback_query',
        'key' => $m['message_id'],
        'date' => $this->d->format($m['date'], 'custom', $this->df),
        'text' => $this->getVal($m, 'text') . " = $data",
        'bot_id' => $this->id,
        'chat_id' => $chat->id(),
        'user_id' => $user->id(),
        'entities' => $this->getVal($m, 'entities'),
        // 'yaml' => Yaml::encode($data, 8),
      ];
      $this->saveMessageEntity($message);
    }
    elseif (!empty($data['my_chat_member'])) {
      $m = $data['my_chat_member'];
      $user = $this->user($m);
      $chat = $this->chat($m);
      $new = $m['new_chat_member'];
      $title = "{$new['user']['username']} ({$new['status']})";
      $message = [
        'title' => "$title | chat_member",
        'key' => $data['update_id'],
        'date' => $this->d->format($m['date'], 'custom', $this->df),
        'text' => $new['status'],
        'bot_id' => $this->id,
        'chat_id' => $chat->id(),
        'user_id' => $user->id(),
        'entities' => $this->getVal($m, 'entities'),
        'yaml' => Yaml::encode($data, 8),
      ];
      $this->saveMessageEntity($message);
    }
    else {
      \Drupal::logger('telega_bot')->error("bot:" . $bot->id());
      \Drupal::logger('telega_bot')->error(print_r($data, TRUE));
    }
  }

  /**
   * Save Message entity.
   */
  private function saveMessageEntity(array $message) : bool {
    if (\Drupal::moduleHandler()->moduleExists('telega_message')) {
      $storage = $this->etype->getStorage('telega_message');
      $storage->create($message)->save();
      return TRUE;
    }
    else {
      // \Drupal::logger('telega_bot')->info("<pre>{Yaml::encode($message)}</pre>");
      return FALSE;
    }
  }

  /**
   * Skip chatwoot.
   */
  public function setSkipChatwoot(bool $skip) : self {
    $this->skipChatwoot = $skip;
    return $this;
  }

  /**
   * Send Message whith chatwoot.
   */
  private function sendMessage(array $data) : array {
    $json = Request::sendMessage($data);
    if (!$this->skipChatwoot && \Drupal::hasService('telega_chatwoot')) {
      $send_message_result = Json::decode($json);
      if (!empty($send_message_result['result']['chat'])) {
        $this->createUser($send_message_result['result']['chat']);
        $this->chat($send_message_result['result']);
      }
      $message = [
        'inbox_id' => $this->entity->getYamlParam('chatwoot/inbox'),
        'text' => $data['text'],
        'user' => $data['chat_id'],
        'bot' => $this->entity->name->value,
        "message_type" => "outgoing",
        "private" => TRUE,
      ];
      \Drupal::service('telega_chatwoot')
        ->createMessage($message);
    }
    $data = Json::decode($json);
    return $data;
  }

}
