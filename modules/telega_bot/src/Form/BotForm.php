<?php

namespace Drupal\telega_bot\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\telega\Utility\AjaxResult;

/**
 * Implements the form controller.
 */
class BotForm extends FormBase {

  /**
   * Ajax Wrapper.
   *
   * @var string
   */
  private $wrapper = 'telega-bot-form-wrap';

  /**
   * AJAX Version.
   */
  public function ajaxSetup(array &$form, $form_state) {
    $entity = $form_state->entity;
    $name = $entity->title->value;
    $otvet = "setup $name:\n";
    $bot = \Drupal::service('telega_bot.webhook')->init($entity);
    $otvet .= $bot->setup();
    return AjaxResult::ajax($this->wrapper, $otvet);
  }

  /**
   * Build the simple form.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $extra = NULL) {
    $wrap = $this->wrapper;
    $form_state->entity = $extra;
    $form_state->setCached(FALSE);
    $form['main'] = [
      '#type' => 'details',
      '#title' => $this->t('Bot'),
      '#open' => TRUE,
      'setup' => AjaxResult::button('::ajaxSetup', 'Setup'),
    ];
    $form['output'] = [
      '#suffix' => "<div id='$wrap'>console</div>",
    ];
    return $form;
  }

  /**
   * Implements a form submit handler.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $form_state->setRebuild(TRUE);
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'telega_bot_form';
  }

}
