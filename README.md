# Telega

## Survey-команды и Redis

## Chatwoot

- Создаём бота в бот-фазере
- Добавляем бота в друпал,
  - нажимаем `setup` чтобы привязаться к телеграмму
  - берём необходимый урл со страницы бота
- Добавляем источник в чатвут:
  - Имя `tgX: MyBot`
  - Webhook: url со страницы бота вида `https://example.com/telega/chatwoot/webhook/bot/Y`
  - SAVE
  - Нажимаем "редактировать" у источника и смотрим его `ID`, например `3`
  - правим имя на `tgX: MyBot` заменяя `X` на id, наример на `3`
- Добаляем информацию о источнике в бота YAML поле

```yml
commands:
  telegram: MyBot # папка с командами
chatwoot:
  inbox: 3 # id источника в чатвуте
```

- ## Chatwoot / Добавляем необходимые поля `Пользовательские атрибуты` для интеграции:
  - Диалог
    - id[text] - id пользователя/группы (он будет в id чатвута в виде )
    - bot[text] - bot от которого общаемся
    - name[text] - название юзера / группы
    - ?? created[дата] - дата создания
    - ?? last_name[text] -
    - ?? first_name[text]
  - Контакт
    - id[text] - id пользователя/группы
    - name[text] (name телеги)
    - ?? created[дата]
    - ?? last_name[text]
    - ?? first_name[text]
- ## Chatwoot / Если я хочу написать первым в бота:
  - Создаю контакт
  - заполняю телефон
  - заполняю id[text]
- ## Chatwoot / Если я хочу написать первым в телегу от юзера
  - Создаю контакт
  - заполняю телефон
  - заполняю id[text]

## Поля для Статистики (Канал "чат" или кнопка "старт"):

- User-id
- Matomo / Piwik
- Yandex
- Google

## Пробрасывать статистику о посещениях

## MtProto

- Библиотека https://github.com/danog/MadelineProto
  - документация https://docs.madelineproto.xyz/
  - пример https://github.com/danog/pipesbot/blob/master/pipesbot.php
- Сначала нужно создать своё приложение
  - https://my.telegram.org/apps
  - получаем `api_id` и `api_hash`
  - описание процесса https://core.telegram.org/api/obtaining_api_id
- Процесс запуска:
  - #1 запускаем скрипт
  - #2 `m` Manual `Your choice (m/a):`
  - #3 Тут нужен API-кей и ключ
  - #4 `u` User / Do you want to login as user or bot (u/b)?
  - #5 7XXXXXXXXXX | Enter your phone number:
  - #6 XXXXX | Enter the code:
